/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magazzino;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.ConnessioneDatabaseMySql;

/**
 *
 * @author loren
 */

public class CheckMagazzino {

		String sqlQueryCuoio = "", sqlQueryMinuteria = "", sqlQueryChimici = "";
		
		public void CheckMagazzino() {
			sqlQueryCuoio = "SELECT idCuoio AS id, cuNome AS nome, cuTipo AS tipo FROM cuoio WHERE cuQuantita <= '0.3'";
			sqlQueryMinuteria = "SELECT idMinuteria AS id, mnNome AS nome, mnCodice AS codice FROM minuteria WHERE mnQuantita < '3'";
			sqlQueryChimici = "SELECT idChimici AS id, chNome AS nome, chCodice AS codice FROM chimici WHERE chQuantita < '3'";	
		}
		
		public ResultSet CheckCuoio(ConnessioneDatabaseMySql con) throws SQLException {
			
			PreparedStatement ps = con.getConnection().prepareStatement(sqlQueryCuoio);
			
			ResultSet rs = ps.executeQuery();
			return rs;
			
		}
		
		public ResultSet CheckChimici(ConnessioneDatabaseMySql con) throws SQLException {
			
			PreparedStatement ps = con.getConnection().prepareStatement(sqlQueryChimici);
			
			ResultSet rs = ps.executeQuery();
			return rs;
			
		}
		
		public ResultSet CheckMinuteria(ConnessioneDatabaseMySql con) throws SQLException {
			
			PreparedStatement ps = con.getConnection().prepareStatement(sqlQueryMinuteria);
			
			ResultSet rs = ps.executeQuery();
			return rs;
			
		}		
		
}
