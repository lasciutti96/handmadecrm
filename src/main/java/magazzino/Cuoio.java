/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magazzino;

/**
 *
 * @author loren
 */

public class Cuoio extends Item {
	private String tipoCuoio;
	private String colore;
	private float resa;
	private String qualita;
	

	public String getTipoCuoio() {
		return tipoCuoio;
	}
	public void setTipoCuoio(String tipoCuoio) {
		this.tipoCuoio = tipoCuoio;
	}
	public String getColore() {
		return colore;
	}
	public void setColore(String colore) {
		this.colore = colore;
	}
	public float getResa() {
		return resa;
	}
	public void setResa(float resa) {
		this.resa = resa;
	}
	public String getQualita() {
		return qualita;
	}
	public void setQualita(String qualita) {
		this.qualita = qualita;
	}
	
	
}