/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magazzino;

/**
 *
 * @author loren
 */

public class Strumenti {
	private String articolo;
	private String rivenditore;
	private int quantita;
	private float costo;
	
	public String getArticolo() {
		return articolo;
	}
	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}
	public String getRivenditore() {
		return rivenditore;
	}
	public void setRivenditore(String rivenditore) {
		this.rivenditore = rivenditore;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	public float getCosto() {
		return costo;
	}
	public void setCosto(float costo) {
		this.costo = costo;
	}
	
	
}
