/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.protocol.Resultset;

import database.ConnessioneDatabaseMySql;

import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 *
 * @author loren
 */
public class InterfacciaMagazzino {

	protected JFrame frame;
	protected JTable tableCuoio;
	private JTable tableChimici;
	private JTable tableMinuteria;
	private JTable tableAttrezzi;
	private JTable tableMacchinari;

	/**
	 * Create the application.
	 * @throws SQLException 
	 */
	public InterfacciaMagazzino() throws SQLException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings("serial")
	protected void initialize() throws SQLException {
		frame = new JFrame("Magazzino");
		frame.getContentPane().setBackground(new Color(175, 238, 238));
		frame.setBounds(100, 100, 1006, 563);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Magazzino");
		lblNewLabel.setBounds(10, 11, 193, 61);
		lblNewLabel.setForeground(new Color(0, 255, 127));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 32));
		frame.getContentPane().add(lblNewLabel);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tabbedPane.setBounds(10, 96, 972, 268);
		frame.getContentPane().add(tabbedPane);
		
		/**
		 * 		scroll pane di cuoio
		 * */
		JScrollPane cuoioPanel = new JScrollPane();
		tabbedPane.addTab("Cuoio", null, cuoioPanel, null);
		
		/**
		 * 
		 * 		crea le tabelle coi valori del database
		 * */
		
		//Tabella cuoio
		tableCuoio = new JTable();
		tableCuoio.setEnabled(true);
		tableCuoio.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {
				"id", "Nome", "Tipo", "Colore", "Quantit\u00E0", "Qualit\u00E0", "Costo", "Resa", "Rivenditore"
			}
		));
		cuoioPanel.setViewportView(tableCuoio);
		
		JScrollPane chimiciPanel = new JScrollPane();
		tabbedPane.addTab("Chimici", null, chimiciPanel, null);
		
		
		//Tabella chimici
		tableChimici = new JTable();
		tableChimici.setEnabled(false);
		tableChimici.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id", "Nome", "Codice","Colore" , "Costo", "Quantità", "Rivenditore"
			}
		));
		chimiciPanel.setViewportView(tableChimici);
		
		JScrollPane minuteriaPanel = new JScrollPane();
		tabbedPane.addTab("Minuteria", null, minuteriaPanel, null);
		
		
		//Tabella minuteria
		tableMinuteria = new JTable();
		tableMinuteria.setEnabled(false);
		tableMinuteria.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {
				"id", "Nome", "Codice", "Costo", "Resa", "Finitura", "Quantità", "Rivenditore"
			}
		));
		minuteriaPanel.setViewportView(tableMinuteria);
		
		JScrollPane attrezziPanel = new JScrollPane();
		tabbedPane.addTab("Attrezzi", null, attrezziPanel, null);
		
		tableAttrezzi = new JTable();
		tableAttrezzi.setEnabled(false);
		tableAttrezzi.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {
				"id", "Nome", "Quantità", "Costo", "Rivenditore"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		attrezziPanel.setViewportView(tableAttrezzi);
		
		JScrollPane macchinariPanel = new JScrollPane();
		tabbedPane.addTab("Macchinari", null, macchinariPanel, null);
		
		tableMacchinari = new JTable();
		tableMacchinari.setEnabled(false);
		tableMacchinari.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {
				"id", "Nome", "Status", "Costo", "Rivenditore"
			}
		));
		macchinariPanel.setViewportView(tableMacchinari);
		
		JButton addButton = new JButton("Aggiungi ");
		addButton.setBackground(new Color(255, 255, 255));
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//azione per aprire la form del magazzino
				
				try {
					addInterfaccia frame = new addInterfaccia();
					frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		addButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		addButton.setBounds(204, 395, 163, 50);
		frame.getContentPane().add(addButton);
		
		JButton modifyButton = new JButton("Modifica");
		modifyButton.setBackground(new Color(255, 255, 255));
		modifyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modifyInterfaccia frame3 = new modifyInterfaccia();
				frame3.setVisible(true);
			}
		});
		modifyButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		modifyButton.setBounds(417, 395, 163, 50);
		frame.getContentPane().add(modifyButton);
		
		JButton delButton = new JButton("Elimina");
		delButton.setBackground(new Color(255, 255, 255));
		delButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteInterfaccia frame2 = new deleteInterfaccia();
				frame2.setVisible(true);
			}
		});
		delButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		delButton.setBounds(628, 395, 163, 50);
		frame.getContentPane().add(delButton);
		
		/**
		 * 		Creazione vista tabelle
		 * 
		 * */
		setQueryTab();
		
		/**
		 * 		Pulsante refresh tabelle
		 * 
		 * */
		JButton refreshButton = new JButton("Refresh");
		refreshButton.setBackground(new Color(255, 255, 255));
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//refresh tabelle
				try {
					//devo prima ripulire tutte le tabelle
					DefaultTableModel m = (DefaultTableModel) tableCuoio.getModel();
					DefaultTableModel m1 = (DefaultTableModel) tableChimici.getModel();
					DefaultTableModel m2 = (DefaultTableModel) tableMinuteria.getModel();
					DefaultTableModel m3 = (DefaultTableModel) tableAttrezzi.getModel();
					DefaultTableModel m4 = (DefaultTableModel) tableMacchinari.getModel();
					m.setRowCount(0);
					m1.setRowCount(0);
					m2.setRowCount(0);
					m3.setRowCount(0);
					m4.setRowCount(0);
					setQueryTab();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		refreshButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		refreshButton.setBounds(10, 395, 105, 50);
		frame.getContentPane().add(refreshButton);
		
	}
	
	public void setQueryTab() throws SQLException {
		/**
		 * 
		 * 		Setta la query per la tabella cuoio
		 * 
		 * */
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		String sqlQuery = con.setQuery("cuoio","SELECT");
		addRowsSelectQuery(con, sqlQuery, "cuoio");
		
		
		/**
		 * 
		 * 		Setta la query per la tabella chimici
		 * 
		 * */
		
		sqlQuery = con.setQuery("chimici","SELECT");
		addRowsSelectQuery(con, sqlQuery,"chimici");
		
		/**
		 * 
		 * 		Setta la query per la tabella minuteria
		 * 
		 * */
		sqlQuery = con.setQuery("minuteria","SELECT");
		addRowsSelectQuery(con, sqlQuery,"minuteria");
		
		/**
		 * 
		 * 		Setta la query per la tabella attrezzi
		 * 
		 * */
		sqlQuery = con.setQuery("attrezzi","SELECT");
		addRowsSelectQuery(con, sqlQuery,"attrezzi");
		
		/**
		 * 
		 * 		Setta la query per la tabella macchinari
		 * 
		 * */
		sqlQuery = con.setQuery("macchinari","SELECT");
		addRowsSelectQuery(con, sqlQuery,"macchinari");
		
	}
		
		
	
	/**
	 * 
	 * 		Metodo per inserimento dati database nella tabella
	 * 
	 * */
	public void addRowsSelectQuery(ConnessioneDatabaseMySql con, String sql_query, String nomeTabella) throws SQLException{
		
		PreparedStatement ps = con.getConnection().prepareStatement(sql_query);
		
		ResultSet rs = ps.executeQuery();
		
		switch(nomeTabella) {
		
			case ("cuoio"): 
				while(rs.next()) {
					DefaultTableModel m = (DefaultTableModel) tableCuoio.getModel();
					m.addRow(new Object[] {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(8),rs.getString(6),rs.getString(7),rs.getString(4),rs.getString(5),rs.getString(9)});
				}
			break;
			
			case("chimici"):
				while(rs.next()) {
					DefaultTableModel m = (DefaultTableModel) tableChimici.getModel();
					m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getString(7),rs.getString(3),rs.getString(4),rs.getString(6),rs.getString(5)});
				}
			break;
			
			case("minuteria"):
				while(rs.next()) {
					DefaultTableModel m = (DefaultTableModel) tableMinuteria.getModel();
					m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(8),rs.getString(7),rs.getString(6),rs.getString(5)});
				}
			break;
			
			case("attrezzi"):{
				while(rs.next()) {
					DefaultTableModel m = (DefaultTableModel) tableAttrezzi.getModel();
					m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
				}
			}
			break;
			
			case("macchinari"):{
				while(rs.next()) {
					DefaultTableModel m = (DefaultTableModel) tableMacchinari.getModel();
					m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
				}
			}
			break;
		}
		
		ps.close();
		
	}
}
