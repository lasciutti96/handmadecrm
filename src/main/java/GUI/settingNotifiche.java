/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.ParseInfo;

import database.ConnessioneDatabaseMySql;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

/**
 *
 * @author loren
 */

public class settingNotifiche extends JFrame {

	private JPanel contentPane;
	private JTextField txtCu;
	private JTextField txtMn;
	private JTextField txtCh;

	/**
	 * Create the frame.
	 */
	public settingNotifiche() {
		setTitle("Setting");
		setBounds(100, 100, 333, 197);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel cuLabel = new JLabel("Limiti cuoio");
		cuLabel.setBounds(10, 27, 82, 14);
		contentPane.add(cuLabel);
		
		JLabel mnLabel = new JLabel("Limiti Minuteria");
		mnLabel.setBounds(10, 78, 82, 14);
		contentPane.add(mnLabel);
		
		JLabel chLabel = new JLabel("Limiti chimici");
		chLabel.setBounds(10, 135, 82, 14);
		contentPane.add(chLabel);
		
		txtCu = new JTextField();
		txtCu.setBounds(125, 24, 38, 20);
		contentPane.add(txtCu);
		txtCu.setColumns(10);
		
		txtMn = new JTextField();
		txtMn.setBounds(125, 75, 38, 20);
		contentPane.add(txtMn);
		txtMn.setColumns(10);
		
		txtCh = new JTextField();
		txtCh.setBounds(125, 132, 38, 20);
		contentPane.add(txtCh);
		txtCh.setColumns(10);
		
		JButton doneButton = new JButton("Fatto");
		doneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					setSetting(txtCu, txtMn, txtCh);
					dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		doneButton.setBounds(210, 131, 89, 23);
		contentPane.add(doneButton);
	}
	
	public void setSetting(JTextField cu, JTextField mn, JTextField ch) throws SQLException {
		Integer cuoio, minuteria, chimici;
		cuoio = Integer.parseInt(cu.getText());
		minuteria = Integer.parseInt(mn.getText());
		chimici = Integer.parseInt(ch.getText());
		String sqlQuerySetting = "UPDATE impostazioni SET limitiCuoio = '"+cuoio+"',limitiMinuteria = '"+minuteria+"',limitiChimici = '"+chimici+"' WHERE idImpostazioni = '0';";
		//System.out.println(sqlQuerySetting);
		
		updateRowDatabase(sqlQuerySetting);
	}
	
	public void updateRowDatabase(String sqlQuery) throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}

}
