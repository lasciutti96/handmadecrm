/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

import database.ConnessioneDatabaseMySql;

/**
 *
 * @author loren
 */

public class cronologiaPreventivi extends JFrame {

	private JPanel contentPane;
	private JTable cronologiaPreventiviTable;
	

	/**
	 * Create the frame.
	 */
	public cronologiaPreventivi() {
		setTitle("Cronologia preventivi");
		setBounds(100, 100, 1048, 412);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane cronoPane = new JScrollPane();
		cronoPane.setBounds(10, 28, 999, 296);
		contentPane.add(cronoPane);
		
		cronologiaPreventiviTable = new JTable();
		cronologiaPreventiviTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id Cliente", "Nome cliente", "id Prodotto", "Nome prodotto", "id Preventivo", "Numero preventivo", "Data", "Sconto", "Totale", "Status"
			}
		));
		cronologiaPreventiviTable.setBackground(Color.WHITE);
		cronoPane.setViewportView(cronologiaPreventiviTable);
		
		/**
		 * 		Vista dei preventivi con join cliente e prodotto
		 * 
		 * */
		
		try {
			retrivePreventivi();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void retrivePreventivi() throws SQLException{
		
		String sqlQuery = "SELECT C.idCliente, C.clNome, I.idProdotto, I.prArticolo, P.* FROM cliente AS C, prodotto AS I, preventivo AS P " +
							"WHERE C.idCliente = P.idCliente AND I.idProdotto = P.idProdotto AND P.pvStatus = 'non attivo' ORDER BY pvStatus, C.idCliente ASC";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			DefaultTableModel m = (DefaultTableModel) cronologiaPreventiviTable.getModel();
			m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getInt(5),rs.getString(9),rs.getString(6),rs.getString(8),rs.getString(7),rs.getString(11)});
		}
		
		ps.close();
	}
	
}
