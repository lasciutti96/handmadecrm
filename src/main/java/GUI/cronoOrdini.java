/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.ConnessioneDatabaseMySql;
import java.awt.Color;
/**
 *
 * @author loren
 */

public class cronoOrdini extends JFrame {

	private JPanel contentPane;
	private JTable cronologiaTable;


	/**
	 * Create the frame.
	 */
	public cronoOrdini() {
		setTitle("Cronologia ordini");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 781, 329);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(10, 30, 747, 160);
		contentPane.add(scrollPane);
		
		cronologiaTable = new JTable();
		cronologiaTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id Ordine", "Data ordine", "Prodotto", "Cliente", "id Preventivo", "Data consegna", "Consegnato"
			}
		));
		scrollPane.setViewportView(cronologiaTable);
		

		/**
		 * 		Vista dei ordini con join cliente, prodotto e preventivo
		 * 
		 * */
		
		try {
			retriveOrdini();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void retriveOrdini() throws SQLException{
		
		String sqlQuery = "SELECT C.idCliente, C.clNome, C.clCognome, I.idProdotto, I.prArticolo, P.idPreventivo, O.* FROM cliente AS C, prodotto AS I, preventivo AS P, ordine AS O " +
							"WHERE C.idCliente = O.idCliente AND I.idProdotto = O.idProdotto AND P.idPreventivo = O.idPreventivo AND orConsegnato = '1' ORDER BY O.orDataConsegna ASC";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			DefaultTableModel m = (DefaultTableModel) cronologiaTable.getModel();
			String nomeCognome = rs.getString(2)+" "+rs.getString(3);
			m.addRow(new Object[] {rs.getInt(7),rs.getString(8),rs.getString(5),nomeCognome,rs.getInt(11),rs.getString(12),rs.getString(13)});
		}
		
		ps.close();
	}
}
