/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import static javax.swing.JOptionPane.showMessageDialog;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;

/**
 *
 * @author loren
 */
public class addInterfaccia extends JFrame {

	private JPanel contentPane;
	private String nomeTabella;
	private JTextField txtNome;
	private JTextField txtCosto;
	private JTextField txtRivenditore;
	private JTextField txtQuantita;
	private JTextField txtResa;
	private JTextField txtColore;
	private JTextField txtCodice;
	private JTextField txtQualita;
	private JTextField txtTipo;
	private JTextField txtStatus;
	private JTextField txtFinitura;

	/**
	 * Create the frame.
	 */
	public addInterfaccia() {
		setTitle("Aggiungi");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 384, 413);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JComboBox tabellaBox = new JComboBox();
		tabellaBox.setBackground(new Color(255, 255, 255));
		tabellaBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabellaBox.setModel(new DefaultComboBoxModel(new String[] {"Cuoio", "Chimici", "Minuteria", "Attrezzi", "Macchinari"}));
		tabellaBox.setBounds(10, 34, 224, 36);
		contentPane.add(tabellaBox);
		
		/**
		 * 
		 * 		Creazione label e texfield per inserire i dati
		 * 
		 * */
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 95, 49, 14);
		contentPane.add(lblNome);
		
		JLabel lblCosto = new JLabel("Costo");
		lblCosto.setBounds(10, 120, 49, 14);
		contentPane.add(lblCosto);
		
		JLabel lblRivenditore = new JLabel("Rivenditore");
		lblRivenditore.setBounds(10, 145, 73, 14);
		contentPane.add(lblRivenditore);
		
		JLabel lblQuantità = new JLabel("Quantità");
		lblQuantità.setBounds(10, 170, 49, 14);
		contentPane.add(lblQuantità);
		
		JLabel lblResa = new JLabel("Resa");
		lblResa.setBounds(10, 195, 49, 14);
		contentPane.add(lblResa);
		
		JLabel lblColore = new JLabel("Colore");
		lblColore.setBounds(10, 220, 49, 14);
		contentPane.add(lblColore);
		
		JLabel lblCodice = new JLabel("Codice");
		lblCodice.setBounds(10, 245, 49, 14);
		contentPane.add(lblCodice);
		
		JLabel lblQualità = new JLabel("Qualità");
		lblQualità.setBounds(10, 270, 49, 14);
		contentPane.add(lblQualità);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(10, 295, 49, 14);
		contentPane.add(lblTipo);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setBounds(10, 320, 49, 14);
		contentPane.add(lblStatus);
		
		JLabel lblFinitura = new JLabel("Finitura");
		lblFinitura.setBounds(10, 345, 49, 14);
		contentPane.add(lblFinitura);
		
		txtNome = new JTextField();
		txtNome.setBounds(85, 92, 120, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCosto = new JTextField();
		txtCosto.setColumns(10);
		txtCosto.setBounds(85, 117, 120, 20);
		contentPane.add(txtCosto);
		
		txtRivenditore = new JTextField();
		txtRivenditore.setColumns(10);
		txtRivenditore.setBounds(85, 142, 120, 20);
		contentPane.add(txtRivenditore);
		
		txtQuantita = new JTextField();
		txtQuantita.setColumns(10);
		txtQuantita.setBounds(85, 167, 120, 20);
		contentPane.add(txtQuantita);
		
		txtResa = new JTextField();
		txtResa.setColumns(10);
		txtResa.setBounds(85, 192, 120, 20);
		contentPane.add(txtResa);
		
		txtColore = new JTextField();
		txtColore.setColumns(10);
		txtColore.setBounds(85, 217, 120, 20);
		contentPane.add(txtColore);
		
		txtCodice = new JTextField();
		txtCodice.setColumns(10);
		txtCodice.setBounds(85, 242, 120, 20);
		contentPane.add(txtCodice);
		
		txtQualita = new JTextField();
		txtQualita.setColumns(10);
		txtQualita.setBounds(85, 267, 120, 20);
		contentPane.add(txtQualita);
		
		txtTipo = new JTextField();
		txtTipo.setColumns(10);
		txtTipo.setBounds(85, 292, 120, 20);
		contentPane.add(txtTipo);
		
		txtStatus = new JTextField();
		txtStatus.setColumns(10);
		txtStatus.setBounds(85, 317, 120, 20);
		contentPane.add(txtStatus);
		
		txtFinitura = new JTextField();
		txtFinitura.setColumns(10);
		txtFinitura.setBounds(85, 342, 120, 20);
		contentPane.add(txtFinitura);
		
		/**
		 * 		Pulsante per confermare la scelta della tabella e per escludere i textField che
		 * 		non deve usare
		 * 
		 * */
		
		JButton selectedButton = new JButton("OK");
		selectedButton.setBackground(new Color(255, 255, 255));
		selectedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = (String) tabellaBox.getSelectedItem();
				switch(nomeTabella) {
			
				case("Cuoio"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblResa.setEnabled(true);
					lblColore.setEnabled(true);
					lblQualità.setEnabled(true);
					lblTipo.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtResa.setEnabled(true);
					txtColore.setEnabled(true);
					txtQualita.setEnabled(true);
					txtTipo.setEnabled(true);
//disabilita txtfiel e label
					lblCodice.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtCodice.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Chimici"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblColore.setEnabled(true);
					lblCodice.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtColore.setEnabled(true);
					txtCodice.setEnabled(true);
//disabilita txtfiel e label
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					lblResa.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
					txtResa.setEnabled(false);
				break;
				
				case("Minuteria"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblResa.setEnabled(true);
					lblCodice.setEnabled(true);
					lblFinitura.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtResa.setEnabled(true);
					txtCodice.setEnabled(true);
					txtFinitura.setEnabled(true);
//disabilita txtfiel e label					
					lblColore.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtColore.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Attrezzi"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
//disabilita txtfiel e label	
					lblResa.setEnabled(false);
					lblColore.setEnabled(false);
					lblCodice.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtResa.setEnabled(false);
					txtColore.setEnabled(false);
					txtCodice.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Macchinari"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblStatus.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtStatus.setEnabled(true);
//disabilita txtfiel e label
					lblQuantità.setEnabled(false);
					lblResa.setEnabled(false);
					lblColore.setEnabled(false);
					lblCodice.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtQuantita.setEnabled(false);
					txtResa.setEnabled(false);
					txtColore.setEnabled(false);
					txtCodice.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtFinitura.setEnabled(false);
					
				break;					
			}

		}
	});
		selectedButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		selectedButton.setBounds(305, 34, 55, 36);
		contentPane.add(selectedButton);

		/**
		 * 		Pulsante per aggiungere nel database i nuovi valori
		 * 
		 * */
		
		JButton addButton = new JButton("AGGIUNGI");
		addButton.setBackground(new Color(255, 255, 255));
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sqlQuery = null;
				Float costo;
				Float resa;
				Float quantita;
				switch(nomeTabella) {
				
				case("Cuoio"):
					costo = Float.parseFloat(txtCosto.getText()); 
					resa = Float.parseFloat(txtResa.getText());
					quantita = Float.parseFloat(txtQuantita.getText());
					sqlQuery = "INSERT INTO leathercrm.cuoio"
										+"(cuNome, cuCosto, cuRivenditore,cuQuantita,cuResa,cuColore,cuQualita,cuTipo)"
										+"VALUES('"+txtNome.getText()+"','"+costo+"','"+txtRivenditore.getText()
										+"','"+quantita+"','"+resa+"','"+txtColore.getText()
										+"','"+txtQualita.getText()+"','"+txtTipo.getText()+"');";
					break;
				
				case("Chimici"):
					costo = Float.parseFloat(txtCosto.getText()); 
					quantita = Float.parseFloat(txtQuantita.getText());
					sqlQuery = "INSERT INTO leathercrm.chimici"
							+"(chNome, chColore, chCosto,chRivenditore,chQuantita,chCodice)"
							+"VALUES('"+txtNome.getText()+"','"+txtColore.getText()+"','"+costo
							+"','"+txtRivenditore.getText()+"','"+quantita+"','"+txtCodice.getText()+"');";
					break;
				
				case("Minuteria"):
					costo = Float.parseFloat(txtCosto.getText()); 
					resa = Float.parseFloat(txtResa.getText());
					quantita = Float.parseFloat(txtQuantita.getText());
					sqlQuery = "INSERT INTO leathercrm.minuteria"
							+"(mnNome, mnCodice, mnCosto,mnRivenditore,mnQuantita,mnFinitura,mnResa)"
							+"VALUES('"+txtNome.getText()+"','"+txtCodice.getText()+"','"+costo
							+"','"+txtRivenditore.getText()+"','"+quantita+"','"+txtFinitura.getText()
							+"','"+resa+"');";
					break;
				
				case("Attrezzi"):
					costo = Float.parseFloat(txtCosto.getText()); 
					quantita = Float.parseFloat(txtQuantita.getText());
					sqlQuery = "INSERT INTO leathercrm.attrezzi"
							+"(azArticolo, azQuantita, azCosto,azRivenditore)"
							+"VALUES('"+txtNome.getText()+"','"+quantita+"','"+costo
							+"','"+txtRivenditore.getText()+"');";
					break;
				
				case("Macchinari"):
					costo = Float.parseFloat(txtCosto.getText());
					sqlQuery = "INSERT INTO leathercrm.macchinari"
							+"(mcNome, mcStatus, mcCosto,mcRivenditore)"
							+"VALUES('"+txtNome.getText()+"','"+txtStatus.getText()+"','"+costo
							+"','"+txtRivenditore.getText()+"');";
					break;
				}
				
				try {
					//metodo per eseguire la query di insert
					addNewRowDatabase(sqlQuery);
					
					//alert se l'inserimento è andato a buon fine
					showMessageDialog(null, "Inserimento completato");
					
					//chiusura form
					dispose();
					
				} catch (SQLException e1) {
					//e1.printStackTrace();
					
					//aggiungere un alert se è stato un errore
					showMessageDialog(null, "Errore: " +e1.getMessage());
				}
			}
		});
		addButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		addButton.setBounds(240, 332, 120, 38);
		contentPane.add(addButton);
	
	}
	
	public void addNewRowDatabase(String sqlQuery) throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}
}
