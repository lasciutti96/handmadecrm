/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;


/**
 *
 * @author loren
 */
public class addCliente extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCognome;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtIndirizzo;
	private JTextField txtCitta;


	/**
	 * Create the frame.
	 */
	public addCliente() {
		setTitle("Aggiungi cliente");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 417, 366);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setBounds(10, 31, 49, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setBounds(10, 79, 49, 14);
		contentPane.add(lblCognome);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(10, 132, 49, 14);
		contentPane.add(lblTelefono);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 183, 49, 14);
		contentPane.add(lblEmail);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setBounds(10, 234, 49, 14);
		contentPane.add(lblIndirizzo);
		
		JLabel lblCitta = new JLabel("Citt\u00E0");
		lblCitta.setBounds(10, 282, 49, 14);
		contentPane.add(lblCitta);
		
		txtNome = new JTextField();
		txtNome.setBounds(110, 28, 96, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCognome = new JTextField();
		txtCognome.setBounds(110, 76, 96, 20);
		contentPane.add(txtCognome);
		txtCognome.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(110, 129, 96, 20);
		contentPane.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(110, 180, 96, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtIndirizzo = new JTextField();
		txtIndirizzo.setBounds(110, 231, 96, 20);
		contentPane.add(txtIndirizzo);
		txtIndirizzo.setColumns(10);
		
		txtCitta = new JTextField();
		txtCitta.setBounds(110, 279, 96, 20);
		contentPane.add(txtCitta);
		txtCitta.setColumns(10);
		
		JButton addButton = new JButton("Aggiungi");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String nome = null;
					String cognome = null;
					String telefono = null;
					String email = null;
					String indirizzo = null;
					String citta = null;
					
					nome = txtNome.getText();
					cognome = txtCognome.getText();
					telefono = txtTelefono.getText();
					email = txtEmail.getText();
					indirizzo = txtIndirizzo.getText();
					citta = txtCitta.getText();
					
					boolean i;
					i = aggiungiCliente(nome, cognome, telefono, email, indirizzo, citta);
					
					if(i) {
						showMessageDialog(null, "Inserimento completato");
					}
					
					dispose();
					
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
					showMessageDialog(null, "Errore inserimento dati: "+e1.getMessage());
				}
			}
		});
		addButton.setBackground(Color.WHITE);
		addButton.setBounds(288, 278, 89, 23);
		contentPane.add(addButton);
	}
	
	public boolean aggiungiCliente(String nome, String cognome, String telefono, String email, String indirizzo, String citta) throws SQLException{
		//System.out.println("nome: "+nome+" cognome: "+cognome);
		if(nome.equals("") || cognome.equals("")) {
			showMessageDialog(null, "Errore inserimento dati");
			return false;
		}else {
			String sqlQuery = "INSERT INTO cliente (clNome, clCognome, clNTelefono, clEmail, clIndirizzo, clCitta) VALUES ('"+nome+"','"+cognome+"','"+telefono+"','"+email+"','"+indirizzo+"','"+citta+"');";
			
			addNewRowDatabase(sqlQuery);
			
			return true;
		}
		
	}
	
	public void addNewRowDatabase(String sqlQuery) throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}
}
