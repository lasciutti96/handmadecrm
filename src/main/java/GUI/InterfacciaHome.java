/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.mysql.cj.protocol.Resultset;

import database.ConnessioneDatabaseMySql;
import magazzino.CheckMagazzino;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.DefaultListModel;

import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.UIManager;
import javax.swing.JList;

/**
 *
 * @author loren
 */
public class InterfacciaHome {

	private JFrame frmLeathercrm;
	private boolean i;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacciaHome windowHome = new InterfacciaHome();
					windowHome.frmLeathercrm.setVisible(true);				
					
					/**
					 * 
					 *  Connessione database MySql
					 * 
					 * */
					
//					ConnessioneDatabaseMySql db = new ConnessioneDatabaseMySql();
//					db.getConnection();
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfacciaHome() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLeathercrm = new JFrame();
		frmLeathercrm.setBackground(UIManager.getColor("ToolBar.highlight"));
		frmLeathercrm.setTitle("handmadeCRM\r\n");
		frmLeathercrm.getContentPane().setBackground(new Color(175, 238, 238));
		frmLeathercrm.setBounds(100, 100, 970, 556);
		frmLeathercrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLeathercrm.getContentPane().setLayout(null);
		
		JLabel lblTitle = new JLabel("handmadeCRM");
		lblTitle.setForeground(new Color(0, 0, 0));  //(0,255,127)
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 32));
		lblTitle.setBounds(10, 11, 300, 39);
		frmLeathercrm.getContentPane().add(lblTitle);
		
		JScrollPane notifyPanel = new JScrollPane();
		notifyPanel.setBounds(10, 84, 936, 402);
		frmLeathercrm.getContentPane().add(notifyPanel);
		
		JMenuBar menuBar = new JMenuBar();
		frmLeathercrm.setJMenuBar(menuBar);
		
		JMenu organizzazioneMenu = new JMenu("Organizzazione");
		menuBar.add(organizzazioneMenu);
		
		JMenuItem clientiItem = new JMenuItem("Clienti");
		clientiItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientiInterfaccia frame = new clientiInterfaccia();
				frame.setVisible(true);
			}
		});
		organizzazioneMenu.add(clientiItem);
		
		JMenuItem magazzinoItem = new JMenuItem("Magazzino");
		magazzinoItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//azione per aprire la form del magazzino
				
				try {
					InterfacciaMagazzino window = new InterfacciaMagazzino();
					window.frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		organizzazioneMenu.add(magazzinoItem);
		
		JMenu manageOrdMenu = new JMenu("Gestione ordini");
		menuBar.add(manageOrdMenu);
		
		JMenu preventiviMenu = new JMenu("Preventivi");
		manageOrdMenu.add(preventiviMenu);
		
		JMenuItem newPreventivo = new JMenuItem("Nuovo preventivo");
		newPreventivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//azione per aprire la form del nuovo preventivo
				
				try {
					InterfacciaNuovoPreventivo window = new InterfacciaNuovoPreventivo();
					window.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		preventiviMenu.add(newPreventivo);
		
		JMenuItem listaPreventivi = new JMenuItem("Lista preventivi attivi");
		listaPreventivi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listaPreventivi lp = new listaPreventivi();
				lp.setVisible(true);
			}
		});
		preventiviMenu.add(listaPreventivi);
		
		JMenuItem cronologiaPreventivi = new JMenuItem("Cronologia preventivi");
		cronologiaPreventivi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cronologiaPreventivi cp = new cronologiaPreventivi();
				cp.setVisible(true);
			}
		});
		preventiviMenu.add(cronologiaPreventivi);
		
		JMenu ordiniMenu = new JMenu("Ordini");
		manageOrdMenu.add(ordiniMenu);
		
		JMenuItem listaOrdini = new JMenuItem("Lista ordini attivi");
		listaOrdini.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listaOrdiniAttivi lo = new listaOrdiniAttivi() ;
				lo.setVisible(true);

			}
		});
		ordiniMenu.add(listaOrdini);
		
		JMenuItem cronologiaOrdini = new JMenuItem("Cronologia ordini");
		cronologiaOrdini.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cronoOrdini co = new cronoOrdini();
				co.setVisible(true);
			}
		});
		ordiniMenu.add(cronologiaOrdini);
		
		try {
			JList note = Notifiche(notifyPanel);
			
			JLabel lblNotifiche = new JLabel("Notifiche");
			lblNotifiche.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblNotifiche.setBounds(10, 61, 126, 16);
			frmLeathercrm.getContentPane().add(lblNotifiche);

			JButton settingButton = new JButton("Setting");
			settingButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//apre una piccola form per impostare i parametri per le notifiche delle cose che stanno finendo
					settingNotifiche frame = new settingNotifiche();
					frame.setVisible(true);
			
				}
			});
			settingButton.setBackground(Color.WHITE);
			settingButton.setBounds(857, 59, 89, 23);
			frmLeathercrm.getContentPane().add(settingButton);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*
		 * DefaultListModel<String> lm = new DefaultListModel<String>();
		
		while(rs.next()) {
			strTemp = rs.getInt(1) + ": " + rs.getString(2);
			lm.addElement(strTemp);
		}
		
		JList listProduct = new JList();
		listproductPane.setViewportView(listProduct);
		{
			listProduct.setModel(lm);
		}
		 */
		
	}
	
	public JList Notifiche(JScrollPane notifyPanel) throws SQLException {
		String sqlQueryCuoio = "", sqlQueryMinuteria = "", sqlQueryChimici = "";
		
		String set = retrieveSetting();
		
		String[] limiti = set.split(",");
		
		int quantitaCuoio = Integer.parseInt(limiti[0]);
		int quantitaMinuteria = Integer.parseInt(limiti[1]);
		int quantitaChimici = Integer.parseInt(limiti[2]);

		sqlQueryCuoio = "SELECT idCuoio AS id, cuNome AS nome, cuTipo AS tipo FROM cuoio WHERE cuQuantita <= '"+quantitaCuoio+"'";
		sqlQueryMinuteria = "SELECT idMinuteria AS id, mnNome AS nome, mnCodice AS codice FROM minuteria WHERE mnQuantita < '"+quantitaMinuteria+"'";
		sqlQueryChimici = "SELECT idChimici AS id, chNome AS nome, chCodice AS codice FROM chimici WHERE chQuantita < '"+quantitaChimici+"'";	
		
		System.out.println(sqlQueryCuoio+"   "+sqlQueryMinuteria+"   "+sqlQueryChimici);
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		
		DefaultListModel<String> lm = new DefaultListModel<String>();
		String strTemp = "";
		
		PreparedStatement ps1 = con.getConnection().prepareStatement(sqlQueryCuoio);
		ResultSet rs1 = ps1.executeQuery();
		
		lm.addElement("Cuoio: ");
		while(rs1.next()) {
			
			strTemp = rs1.getInt(1) + "__ Nome: " + rs1.getString(2) + "  Tipo: " +rs1.getString(3) +" sta finendo!";
			//System.out.println(strTemp);
			lm.addElement(strTemp);			
		}
		ps1.close();
		
		PreparedStatement ps2 = con.getConnection().prepareStatement(sqlQueryChimici);
		ResultSet rs2 = ps2.executeQuery();
		
		lm.addElement("Chimici: ");
		while(rs2.next()) {
			
			strTemp = rs2.getInt(1) + "__ Nome: " + rs2.getString(2) + "  Codice: " +rs2.getString(3) +" sta finendo!"; 
			lm.addElement(strTemp);			
		}
		ps2.close();
		
		PreparedStatement ps3 = con.getConnection().prepareStatement(sqlQueryMinuteria);
		ResultSet rs3 = ps3.executeQuery();		
		lm.addElement("Minuteria: ");
		while(rs3.next()) {
			
			strTemp = rs3.getInt(1) + "__ Nome: " + rs3.getString(2) + "  Codice: " +rs3.getString(3) +" sta finendo!"; 
			lm.addElement(strTemp);			
		}
		
		JList notificheLista = new JList();
		notifyPanel.setViewportView(notificheLista);
		{
			notificheLista.setModel(lm);
		}
                return notificheLista;
	}
	
	public String retrieveSetting() throws SQLException{
	
			String sqlQuerySetting = "SELECT limitiCuoio, limitiMinuteria, limitiChimici FROM impostazioni WHERE idImpostazioni = '0'";
			
			ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
			
			PreparedStatement ps = con.getConnection().prepareStatement(sqlQuerySetting);
			
			ResultSet rs = ps.executeQuery();
			String string = null;
			
			if(rs.next())
				string = String.valueOf(rs.getInt(1))+","+String.valueOf(rs.getInt(2))+","+String.valueOf(rs.getInt(3));
			
			ps.close();
			
			return string;
			
			
			
	}
}
