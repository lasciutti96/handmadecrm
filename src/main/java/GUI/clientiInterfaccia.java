/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

//import com.ibm.db2.jcc.am.Connection;
//import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement;

import database.ConnessioneDatabaseMySql;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;


/**
 *
 * @author loren
 */

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

//import com.ibm.db2.jcc.am.Connection;
//import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement;

import database.ConnessioneDatabaseMySql;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class clientiInterfaccia extends JFrame {

	private JPanel contentPane;
	private JTable tableClienti;
	private JTextField txtIdClienti;

	/**
	 * Create the frame.
	 */
	public clientiInterfaccia() {
			
		setTitle("Clienti");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 627, 459);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 593, 191);
		contentPane.add(scrollPane);
		
		tableClienti = new JTable();
		tableClienti.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"idClienti", "Nome", "Cognome", "Telefono", "Email", "Indirizzo", "Città"
			}
		));
		scrollPane.setViewportView(tableClienti);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.setBackground(new Color(255, 255, 255));
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel m = (DefaultTableModel) tableClienti.getModel();
				m.setRowCount(0);
				try {
					createTableClienti();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnRefresh.setBounds(10, 213, 89, 23);
		contentPane.add(btnRefresh);
		
		JButton btnAggiungi = new JButton("Aggiungi");
		btnAggiungi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AGGIUNGERE codice che chiama la form per aggiungere un cliente
				
				try {
					addCliente frame = new addCliente();
					frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnAggiungi.setBackground(new Color(255, 255, 255));
		btnAggiungi.setBounds(10, 259, 89, 23);
		contentPane.add(btnAggiungi);
		
		JButton btnModifica = new JButton("Modifica");
		btnModifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AGGIUNGERE codice che chiama la form per modificare un cliente
				
				try {
					modificaCliente frame = new modificaCliente();
					frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnModifica.setBackground(new Color(255, 255, 255));
		btnModifica.setBounds(10, 293, 89, 23);
		contentPane.add(btnModifica);
		

		txtIdClienti = new JTextField();
		txtIdClienti.setBounds(119, 353, 52, 20);
		contentPane.add(txtIdClienti);
		txtIdClienti.setColumns(10);
		
		JButton btnElimina = new JButton("Elimina");
		btnElimina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//AGGIUNGERE codice che elimina il cliente selezionato dall'id inserito nel textfield
				
				try {
					deleteClienti();
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnElimina.setBackground(new Color(255, 255, 255));
		btnElimina.setBounds(10, 352, 89, 23);
		contentPane.add(btnElimina);		
		
		JLabel lblIdClienti = new JLabel("idClienti");
		lblIdClienti.setBounds(119, 333, 49, 14);
		contentPane.add(lblIdClienti);
		
		try {
			createTableClienti();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			showMessageDialog(null, "Errore: "+e.getMessage());
		}
		
		}
	
	public void createTableClienti() throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		String sqlQuery = "SELECT * FROM cliente";
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ResultSet rs = ps.executeQuery();
		
		DefaultTableModel m = (DefaultTableModel) tableClienti.getModel();
		while(rs.next()) {
			m.addRow(new Object[] {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7)});
		}
		
		ps.close();
	}
	
	public void deleteClienti() throws SQLException {
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		String sqlQuery = "DELETE FROM cliente WHERE idCliente = '" +Integer.parseInt(txtIdClienti.getText())+ "';";
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		int i = ps.executeUpdate();
		
		if(i != 0) {
			showMessageDialog(null, "Cliente eliminato");
		}
		else {
			showMessageDialog(null, "Errore, cliente non eliminato");
		}
		
	}
}
