/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
/**
 *
 * @author loren
 */


public class modificaCliente extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCognome;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtIndirizzo;
	private JTextField txtCitta;
	private JTextField txtId;
	private JLabel idLabel;

	/**
	 * Create the frame.
	 */
	public modificaCliente() {
		setTitle("Modifica cliente");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 446, 305);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel nomeLabel = new JLabel("Nome");
		nomeLabel.setBounds(10, 33, 49, 14);
		contentPane.add(nomeLabel);
		
		JLabel cognomeLabel = new JLabel("Cognome");
		cognomeLabel.setBounds(10, 65, 49, 14);
		contentPane.add(cognomeLabel);
		
		JLabel telefonoLabel = new JLabel("Telefono");
		telefonoLabel.setBounds(10, 103, 49, 14);
		contentPane.add(telefonoLabel);
		
		JLabel emailLabel = new JLabel("Email");
		emailLabel.setBounds(10, 142, 49, 14);
		contentPane.add(emailLabel);
		
		JLabel indirizzoLabel = new JLabel("Indirizzo");
		indirizzoLabel.setBounds(10, 184, 49, 14);
		contentPane.add(indirizzoLabel);
		
		JLabel cittaLabel = new JLabel("Citt\u00E0");
		cittaLabel.setBounds(10, 224, 49, 14);
		contentPane.add(cittaLabel);
		
		txtNome = new JTextField();
		txtNome.setBounds(114, 30, 96, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtCognome = new JTextField();
		txtCognome.setBounds(114, 62, 96, 20);
		contentPane.add(txtCognome);
		txtCognome.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(114, 100, 96, 20);
		contentPane.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(114, 139, 96, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtIndirizzo = new JTextField();
		txtIndirizzo.setBounds(114, 181, 96, 20);
		contentPane.add(txtIndirizzo);
		txtIndirizzo.setColumns(10);
		
		txtCitta = new JTextField();
		txtCitta.setBounds(114, 221, 96, 20);
		contentPane.add(txtCitta);
		txtCitta.setColumns(10);
		
		JButton modificaButton = new JButton("Modifica");
		modificaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					modificaCliente(txtNome, txtCognome, txtTelefono, txtEmail, txtIndirizzo, txtCitta, txtId);
					showMessageDialog(null, "Modifica effettuata");
					dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
					showMessageDialog(null, "Errore: "+e1.getMessage());
				}
			}
		});
		modificaButton.setBackground(Color.WHITE);
		modificaButton.setBounds(335, 220, 89, 23);
		contentPane.add(modificaButton);
		
		txtId = new JTextField();
		txtId.setBounds(328, 30, 96, 20);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		idLabel = new JLabel("id Cliente");
		idLabel.setBounds(254, 33, 49, 14);
		contentPane.add(idLabel);
	}
	
	public void modificaCliente(JTextField nome, JTextField cognome, JTextField telefono, JTextField email, JTextField indirizzo, JTextField citta, JTextField id) throws SQLException{
		String sqlQueryModifica = null;
		
		sqlQueryModifica = "UPDATE cliente SET ";
		if(!(nome.getText().equals("") || nome.getText().equals(null)))
			sqlQueryModifica += "clNome= '" +nome.getText()+"',";
		if(!(cognome.getText().equals("") || cognome.getText().equals(null)))
			sqlQueryModifica += "clCognome= '" +cognome.getText()+"',";
		if(!(telefono.getText().equals("") || telefono.getText().equals(null)))
			sqlQueryModifica += "clNTelefono= '" +telefono.getText()+"',";
		if(!(email.getText().equals("") || email.getText().equals(null)))
			sqlQueryModifica += "clEmail= '" +email.getText()+"',";
		if(!(indirizzo.getText().equals("") || indirizzo.getText().equals(null)))
			sqlQueryModifica += "clIndirizzo= '" +indirizzo.getText()+"',";
		if(!(citta.getText().equals("") || citta.getText().equals(null)))
			sqlQueryModifica += "clCitta= '" +citta.getText()+"',";
		
		sqlQueryModifica = sqlQueryModifica.replaceAll(",$", "");
		
		sqlQueryModifica += " WHERE idCliente = '"+id.getText()+"';";
			
		//System.out.println(sqlQueryModifica);
		modRowDatabase(sqlQueryModifica);
			
	}
	
public void modRowDatabase(String sqlQuery) throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}

}
