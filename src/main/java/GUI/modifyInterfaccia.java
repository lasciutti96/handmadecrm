/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;

import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.event.InputMethodListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.InputMethodEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Color;
/**
 *
 * @author loren
 */


public class modifyInterfaccia extends JFrame {

	private JPanel contentPane;
	private String nomeTabella;
	private JTextField txtNome;
	private JTextField txtCosto;
	private JTextField txtRivenditore;
	private JTextField txtQuantita;
	private JTextField txtResa;
	private JTextField txtColore;
	private JTextField txtCodice;
	private JTextField txtQualita;
	private JTextField txtTipo;
	private JTextField txtStatus;
	private JTextField txtFinitura;
	
	private String nome, rivenditore, tipo, qualita, colore, codice, status, finitura;
	private Float costo, resa, quantita;
	private Integer id;

	private JTextField txtId;

	/**
	 * Create the frame.
	 */
	public modifyInterfaccia() {
		setTitle("Modifica magazzino");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 384, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel contentPane_1 = new JPanel();
		contentPane_1.setBackground(new Color(175, 238, 238));
		contentPane_1.setLayout(null);
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane_1.setBounds(0, 0, 370, 376);
		contentPane.add(contentPane_1);
		
		JComboBox tabellaBox = new JComboBox();
		tabellaBox.setModel(new DefaultComboBoxModel(new String[] {"Cuoio", "Chimici", "Minuteria", "Attrezzi", "Macchinari"}));
		tabellaBox.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tabellaBox.setBounds(10, 20, 224, 36);
		contentPane_1.add(tabellaBox);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 95, 49, 14);
		contentPane_1.add(lblNome);
		
		JLabel lblCosto = new JLabel("Costo");
		lblCosto.setBounds(10, 120, 49, 14);
		contentPane_1.add(lblCosto);
		
		JLabel lblRivenditore = new JLabel("Rivenditore");
		lblRivenditore.setBounds(10, 145, 73, 14);
		contentPane_1.add(lblRivenditore);
		
		JLabel lblQuantità = new JLabel("Quantit\u00E0");
		lblQuantità.setBounds(10, 170, 49, 14);
		contentPane_1.add(lblQuantità);
		
		JLabel lblResa = new JLabel("Resa");
		lblResa.setBounds(10, 195, 49, 14);
		contentPane_1.add(lblResa);
		
		JLabel lblColore = new JLabel("Colore");
		lblColore.setBounds(10, 220, 49, 14);
		contentPane_1.add(lblColore);
		
		JLabel lblCodice = new JLabel("Codice");
		lblCodice.setBounds(10, 245, 49, 14);
		contentPane_1.add(lblCodice);
		
		JLabel lblQualità = new JLabel("Qualit\u00E0");
		lblQualità.setBounds(10, 270, 49, 14);
		contentPane_1.add(lblQualità);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(10, 295, 49, 14);
		contentPane_1.add(lblTipo);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setBounds(10, 320, 49, 14);
		contentPane_1.add(lblStatus);
		
		JLabel lblFinitura = new JLabel("Finitura");
		lblFinitura.setBounds(10, 345, 49, 14);
		contentPane_1.add(lblFinitura);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(85, 92, 120, 20);
		contentPane_1.add(txtNome);
		
		txtCosto = new JTextField();
		txtCosto.setColumns(10);
		txtCosto.setBounds(85, 117, 120, 20);
		contentPane_1.add(txtCosto);
		
		txtRivenditore = new JTextField();
		txtRivenditore.setColumns(10);
		txtRivenditore.setBounds(85, 142, 120, 20);
		contentPane_1.add(txtRivenditore);
		
		txtQuantita = new JTextField();
		txtQuantita.setColumns(10);
		txtQuantita.setBounds(85, 167, 120, 20);
		contentPane_1.add(txtQuantita);
		
		txtResa = new JTextField();
		txtResa.setColumns(10);
		txtResa.setBounds(85, 192, 120, 20);
		contentPane_1.add(txtResa);
		
		txtColore = new JTextField();
		txtColore.setColumns(10);
		txtColore.setBounds(85, 217, 120, 20);
		contentPane_1.add(txtColore);
		
		txtCodice = new JTextField();
		txtCodice.setColumns(10);
		txtCodice.setBounds(85, 242, 120, 20);
		contentPane_1.add(txtCodice);
		
		txtQualita = new JTextField();
		txtQualita.setColumns(10);
		txtQualita.setBounds(85, 267, 120, 20);
		contentPane_1.add(txtQualita);
		
		txtTipo = new JTextField();
		txtTipo.setColumns(10);
		txtTipo.setBounds(85, 292, 120, 20);
		contentPane_1.add(txtTipo);
		
		txtStatus = new JTextField();
		txtStatus.setColumns(10);
		txtStatus.setBounds(85, 317, 120, 20);
		contentPane_1.add(txtStatus);
		
		txtFinitura = new JTextField();
		txtFinitura.setColumns(10);
		txtFinitura.setBounds(85, 342, 120, 20);
		contentPane_1.add(txtFinitura);
		
		JButton okButton = new JButton("OK");
		okButton.setBackground(new Color(255, 255, 255));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = (String) tabellaBox.getSelectedItem();
				switch(nomeTabella) {
				
				case("Cuoio"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblResa.setEnabled(true);
					lblColore.setEnabled(true);
					lblQualità.setEnabled(true);
					lblTipo.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtResa.setEnabled(true);
					txtColore.setEnabled(true);
					txtQualita.setEnabled(true);
					txtTipo.setEnabled(true);
//disabilita txtfiel e label
					lblCodice.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtCodice.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Chimici"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblColore.setEnabled(true);
					lblCodice.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtColore.setEnabled(true);
					txtCodice.setEnabled(true);
//disabilita txtfield e label
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					lblResa.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
					txtResa.setEnabled(false);
				break;
				
				case("Minuteria"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					lblResa.setEnabled(true);
					lblCodice.setEnabled(true);
					lblFinitura.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
					txtResa.setEnabled(true);
					txtCodice.setEnabled(true);
					txtFinitura.setEnabled(true);
//disabilita txtfiel e label					
					lblColore.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtColore.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Attrezzi"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblQuantità.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtQuantita.setEnabled(true);
//disabilita txtfiel e label	
					lblResa.setEnabled(false);
					lblColore.setEnabled(false);
					lblCodice.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblStatus.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtResa.setEnabled(false);
					txtColore.setEnabled(false);
					txtCodice.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtStatus.setEnabled(false);
					txtFinitura.setEnabled(false);
				break;
				
				case("Macchinari"):
//abilita txtfield e label
					lblNome.setEnabled(true);
					lblCosto.setEnabled(true);
					lblRivenditore.setEnabled(true);
					lblStatus.setEnabled(true);
					txtNome.setEnabled(true);
					txtCosto.setEnabled(true);
					txtRivenditore.setEnabled(true);
					txtStatus.setEnabled(true);
//disabilita txtfiel e label
					lblQuantità.setEnabled(false);
					lblResa.setEnabled(false);
					lblColore.setEnabled(false);
					lblCodice.setEnabled(false);
					lblQualità.setEnabled(false);
					lblTipo.setEnabled(false);
					lblFinitura.setEnabled(false);
					txtQuantita.setEnabled(false);
					txtResa.setEnabled(false);
					txtColore.setEnabled(false);
					txtCodice.setEnabled(false);
					txtQualita.setEnabled(false);
					txtTipo.setEnabled(false);
					txtFinitura.setEnabled(false);
					
				break;					
			}

		}
			
	});
		okButton.setFont(new Font("Tahoma", Font.PLAIN, 10));
		okButton.setBounds(305, 22, 55, 36);
		contentPane_1.add(okButton);
		
		JButton modButton = new JButton("MODIFICA");
		modButton.setBackground(new Color(255, 255, 255));
		modButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sqlQuery = null;	
				
				recoveryTxtValue();
				
				switch(nomeTabella) {
				case("Cuoio"):
					sqlQuery = updateQueryUse() + "'" +id+ "'";
					break;
				
				case("Chimici"):
					sqlQuery = updateQueryUse() + "'" +id+ "'";
					break;
				
				case("Minuteria"):
					sqlQuery = updateQueryUse() + "'" +id+ "'";
					break;
				
				case("Attrezzi"):
					sqlQuery = updateQueryUse() + "'" +id+ "'";
					break;
				
				case("Macchinari"):
					sqlQuery = updateQueryUse() + "'" +id+ "'";
					break;
				}
				
				//System.out.println(sqlQuery);
				
				try {
					//metodo per eseguire la query di insert
					modRowDatabase(sqlQuery);
					//alert se l'inserimento è andato a buon fine
					showMessageDialog(null, "Modifica eseguita");
					//chiusura form
					dispose();
				} catch (SQLException e1) {
					//e1.printStackTrace();
					
					//aggiungere un alert se è stato un errore
					showMessageDialog(null, "Errore: " +e1.getMessage());
				}
			}
		});
		modButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		modButton.setBounds(246, 332, 114, 38);
		contentPane_1.add(modButton);
		
		JLabel labelId = new JLabel("Inserisci l'id");
		labelId.setBounds(287, 69, 73, 14);
		contentPane_1.add(labelId);
		
		txtId = new JTextField();
		txtId.setBounds(263, 92, 96, 20);
		contentPane_1.add(txtId);
		txtId.setColumns(10);
	
		
	}
	
	/**
	 * 
	 * 	Metodo per recuperare i valorei dei JtextField
	 * 
	 * 
	 * */
	public void recoveryTxtValue() {
		nome = txtNome.getText();
		rivenditore = txtRivenditore.getText();
		tipo = txtTipo.getText();
		qualita = txtQualita.getText();
		colore = txtColore.getText();
		codice = txtCodice.getText();
		status = txtStatus.getText();
		finitura = txtFinitura.getText();
		
		try {
			if(!(txtCosto.getText().equals(null)|| txtCosto.getText().equals("")))
				costo = Float.parseFloat(txtCosto.getText()); 
		}
		catch(NumberFormatException e) {
			//System.out.println("eccezione costo");
		}
		try {
			if(!(txtResa.getText().equals(null)|| txtResa.getText().equals("")))
				resa = Float.parseFloat(txtResa.getText());
		}
		catch(NumberFormatException e) {
			//System.out.println("eccezione resa");
		}
		try {
			if(!(txtQuantita.getText().equals(null) || txtQuantita.getText().equals("")))
				quantita = Float.parseFloat(txtQuantita.getText());
		}
		catch(NumberFormatException e) {
			//System.out.println("eccezione quantita");
		}
		try {
			if(!(txtId.getText().equals(null) || txtId.getText().equals("")))
				id = Integer.parseInt(txtId.getText());
			else
				showMessageDialog(null, "Inserire l'id");
		}
		catch(NumberFormatException e) {
			//System.out.println("eccezione id");
		}
		
	}
	
	
	
	
	/**
	 * 
	 * 
	 * 		Metodo per creare la query di update corretta
	 * 
	 * 
	 * */
	private String updateQueryUse () {
				String sqlQuery = null;
				switch(nomeTabella) {
				case("Cuoio"):

					sqlQuery = "UPDATE cuoio SET ";
					
					if(!(nome.equals(null) || nome.equals("")))
						sqlQuery += "cuNome = '" +nome+"',";
					if(!(tipo.equals(null) || tipo.equals("")))
						sqlQuery += "cuTipo = '" +tipo+"',";
					if(!(costo == null))
						sqlQuery += "cuCosto = '" +costo+"',";
					if(!(resa == null))
						sqlQuery += "cuResa = '" +resa+"',";
					if(!(quantita == null))
						sqlQuery += "cuQuantita = '" +quantita+"',";
					if(!(qualita.equals(null) || qualita.equals("")))
						sqlQuery += "cuQualita = '" +qualita+"',";
					if(!(colore.equals(null) || colore.equals("")))
						sqlQuery += "cuColore = '" +colore+"',";
					if(!(rivenditore.equals(null) || rivenditore.equals("")))
						sqlQuery += "cuRivenditore = '" +rivenditore+"',";
					
					sqlQuery = sqlQuery.replaceAll(",$", "");
					
					sqlQuery += " WHERE idCuoio = ";
					
					break;
					
				case("Chimici"):

					sqlQuery = "UPDATE chimici SET ";
				
					if(!(nome.equals(null) || nome.equals(""))) 
						sqlQuery += "chNome= '" +nome+"',";
					if(!(colore.equals(null) || colore.equals("")))
							sqlQuery += "chColore= '" +colore+"',";
					if((costo != null)) 
						sqlQuery += "chCosto= '" +costo+"',";
					if(!(rivenditore.equals(null) || rivenditore.equals("")))
							sqlQuery += "chRivenditore= '" +rivenditore+"',";
					if(!(quantita == null)) 
						sqlQuery += "chQuantita= '" +quantita+"',";
					if(!(codice.equals(null) || codice.equals("")))
						sqlQuery += "chCodice= '" +codice+"',";
						
					sqlQuery = sqlQuery.replaceAll(",$", "");
					
					sqlQuery += " WHERE idChimici = ";
					break;
				
				case("Minuteria"):
					sqlQuery = "UPDATE minuteria SET ";
				
					if(!(nome.equals(null) || nome.equals("")))
							sqlQuery += "mnNome= '" +nome+"',";
					if(!(codice.equals(null) || codice.equals("")))
							sqlQuery += "mnCodice= '" +codice+"',";
					if(!(costo == null))
							sqlQuery += "mnCosto= '" +costo+"',";
					if(!(rivenditore.equals(null) || rivenditore.equals("")))
							sqlQuery += "mnRivenditore= '" +rivenditore+"',";
					if(!(quantita == null))
							sqlQuery += "mnQuantita= '" +quantita+"',";
					if(!(finitura.equals(null) || finitura.equals("")))
							sqlQuery += "mnFinitura= '" +finitura+"',";
					if(!(resa == null))
							sqlQuery += "mnResa= '" +resa+"',";
					
					sqlQuery = sqlQuery.replaceAll(",$", "");
					
					sqlQuery += " WHERE idMinuteria = ";
					break;
				
				case("Attrezzi"):
					sqlQuery = "UPDATE attrezzi SET ";
				
					if(!(nome.equals(null) || nome.equals("")))
						sqlQuery += "azArticolo= '" +nome+"',";
					if(!(quantita == null))
							sqlQuery += "azQuantita= '" +quantita+"',";
					if(!(costo == null))
							sqlQuery += "azCosto= '" +costo+"',";
					if(!(rivenditore.equals(null) || rivenditore.equals("")))
							sqlQuery += "azRivenditore= '" +rivenditore+"',";
					
					sqlQuery = sqlQuery.replaceAll(",$", "");
					
					sqlQuery += " WHERE idAttrezzi = ";
					break;
				
				case("Macchinari"):
					sqlQuery = "UPDATE macchinari SET ";
					
					if(!(nome.equals(null) || nome.equals("")))
						sqlQuery += "mcNome= '" +nome+"',";
					if(!(status.equals(null) || status.equals("")))
							sqlQuery += "mcStatus= '" +status+"',";
					if(!(costo == null))
						sqlQuery += "mcCosto= '" +costo+"',";
					if(!(rivenditore.equals(null) || rivenditore.equals("")))
						sqlQuery += "mcRivenditore= '" +rivenditore+"',";
					
					sqlQuery = sqlQuery.replaceAll(",$", "");
					
					sqlQuery += " WHERE idMacchinari = ";
					break;
				}
				
				return sqlQuery;
				
		}
	
public void modRowDatabase(String sqlQuery) throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}
}
