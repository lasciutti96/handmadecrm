/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.protocol.Resultset;

import database.ConnessioneDatabaseMySql;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
/**
 *
 * @author loren
 */


public class listaPreventivi extends JFrame {

	private JPanel contentPane;
	private JTable tablePreventivi;
	private JTextField txtDisattiva;
	private JTextField txtOrdine;

	
	/**
	 * Create the frame.
	 */
	public listaPreventivi() {
		setTitle("Lista preventivi attivi");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 828, 481);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane preventiviPane = new JScrollPane();
		preventiviPane.setBounds(10, 11, 794, 249);
		contentPane.add(preventiviPane);
		
		tablePreventivi = new JTable();
		tablePreventivi.setBackground(Color.WHITE);
		tablePreventivi.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id Cliente", "Nome cliente", "id Prodotto", "Nome prodotto","Numero di prodotti", "id Preventivo", "Numero preventivo", "Data", "Sconto", "Totale", "Status"
			}
		));
		preventiviPane.setViewportView(tablePreventivi);
		
		JLabel lblDisattiva = new JLabel("Disattiva preventivo");
		lblDisattiva.setBounds(10, 301, 124, 14);
		contentPane.add(lblDisattiva);
		
		txtDisattiva = new JTextField();
		txtDisattiva.setBounds(130, 298, 47, 20);
		contentPane.add(txtDisattiva);
		txtDisattiva.setColumns(10);
		
		JButton btnDisattiva = new JButton("ok");
		btnDisattiva.setBackground(new Color(255, 255, 255));
		btnDisattiva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					disattivaPreventivo();
				} catch (SQLException e1) {
					
					showMessageDialog(null, "Errore: " +e1.getMessage());
				}
			}
		});
		btnDisattiva.setBounds(192, 297, 34, 23);
		contentPane.add(btnDisattiva);
		
		JLabel lblCreaOrdine = new JLabel("Crea Ordine");
		lblCreaOrdine.setBounds(10, 352, 96, 14);
		contentPane.add(lblCreaOrdine);
		
		JButton btnOrdine = new JButton("New button");
		btnOrdine.setBackground(new Color(255, 255, 255));
		btnOrdine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					creaOrdine();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnOrdine.setBounds(192, 348, 34, 23);
		contentPane.add(btnOrdine);
		
		txtOrdine = new JTextField();
		txtOrdine.setBounds(130, 349, 47, 20);
		contentPane.add(txtOrdine);
		txtOrdine.setColumns(10);
		
		JLabel lblIdPreventivo = new JLabel("idPreventivo");
		lblIdPreventivo.setBounds(130, 271, 69, 14);
		contentPane.add(lblIdPreventivo);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.setBackground(new Color(255, 255, 255));
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//refresh tabelle
				try {
					//devo prima ripulire tutte le tabelle
					DefaultTableModel m = (DefaultTableModel) tablePreventivi.getModel();
					
					m.setRowCount(0);
					
					retrivePreventivi();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnRefresh.setBounds(715, 292, 89, 23);
		contentPane.add(btnRefresh);
		
		/**
		 * 		Vista dei preventivi con join cliente e prodotto
		 * 
		 * */
		
		try {
			retrivePreventivi();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void retrivePreventivi() throws SQLException{
		
		String sqlQuery = "SELECT C.idCliente, C.clNome, I.idProdotto, I.prArticolo, P.* FROM cliente AS C, prodotto AS I, preventivo AS P " +
							"WHERE C.idCliente = P.idCliente AND I.idProdotto = P.idProdotto AND P.pvStatus = 'attivo' ORDER BY pvStatus, C.idCliente ASC";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			DefaultTableModel m = (DefaultTableModel) tablePreventivi.getModel();
			m.addRow(new Object[] {rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getInt(14),rs.getInt(5),rs.getString(9),rs.getString(6),rs.getString(8),rs.getString(7),rs.getString(11)});
		}
		
		ps.close();
	}
	
	public void disattivaPreventivo() throws SQLException {
		
		
		String sqlQuery = "UPDATE preventivo SET pvStatus = 'non attivo' WHERE idPreventivo = '" +Integer.parseInt(txtDisattiva.getText())+ "';";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		int i = ps.executeUpdate();
		
		ps.close();
		
		if(i != 0) {
			//aggiungere un alert tutto ok
			showMessageDialog(null, "Preventivo disattivato");
		}
		else {
			showMessageDialog(null, "Preventivo inesistente");
		}
		
		
	}
	
	public void creaOrdine() throws SQLException {
		String sqlQuery1 = "SELECT * FROM preventivo WHERE idPreventivo = '" + Integer.parseInt(txtOrdine.getText()) + "';";
		String sqlQuery2 = null;
		String dataOrdine = null;
		Integer idProdotto = null , idCliente = null, idPreventivo = null;
		
		String r1, r2;
		
		//System.out.println(sqlQuery1);
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery1);
		
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			dataOrdine = rs.getString(2);
			idProdotto = Integer.parseInt(rs.getString(9));
			idCliente = Integer.parseInt(rs.getString(8));
			idPreventivo = Integer.parseInt(rs.getString(1));
		}
		
		ps.close();		
		
		r1 = aggiornaMinuteria(idProdotto);
		r2 = aggiornaCuoio(idProdotto);
		
		
		
		if(r1 == null || r2 == null) {
			//aggiungere un alert tutto ok
			showMessageDialog(null, "Ordine non creato, si è verificato un errore");
		}
		else {
			
			int c = 0;
			String[] queryMinuteria = new String[3];
			
			String[] queryCuoio = new String[3];
			
			queryMinuteria = r1.split(";");
			
			queryCuoio = r2.split(";");
			
			
			try{
				if(queryMinuteria[0] != "" && queryCuoio[0] != "") {
				updateTabelle(queryMinuteria[0], queryCuoio[0], con);
				}
			}
			catch(SQLSyntaxErrorException e1) {}
			catch(ArrayIndexOutOfBoundsException i1) {}
			
			try {
				if(queryMinuteria[1] != "" && queryCuoio[1] != "") {
				updateTabelle(queryMinuteria[1], queryCuoio[1], con);
				}
			}
			catch(SQLSyntaxErrorException e2) {}
			catch(ArrayIndexOutOfBoundsException i2) {}
			
			try {
				if(queryMinuteria[2] != "" && queryCuoio[2] != "") {
				updateTabelle(queryMinuteria[2], queryCuoio[2], con);
				}
			}
			catch(SQLSyntaxErrorException e3) {}
			catch(ArrayIndexOutOfBoundsException i3) {}
			
			
			sqlQuery2 = "INSERT INTO ordine (orDataOrdine, idProdotto, idCliente, idPreventivo, orConsegnato) "
					+ "VALUES ('"+dataOrdine+"','"+idProdotto+"','"+idCliente+"','"+idPreventivo+"','0');";
			
			PreparedStatement ps2 = con.getConnection().prepareStatement(sqlQuery2);
			
			ps2.executeUpdate();
			ps2.close();
			
			System.out.println(sqlQuery2);
			
			String sqlQuery3 = "UPDATE preventivo SET pvStatus = 'non attivo' WHERE idPreventivo = '"+idPreventivo+"';";
			
			PreparedStatement ps3 = con.getConnection().prepareStatement(sqlQuery3);
			
			ps3.executeUpdate();
			ps3.close();
			
			System.out.println(sqlQuery3);
			
			showMessageDialog(null, "Ordine creato");
		}		
		
		
	}
	
	public void updateTabelle (String queryMinuteria, String queryCuoio, ConnessioneDatabaseMySql con) throws SQLException {
		
		PreparedStatement psm = con.getConnection().prepareStatement(queryMinuteria);
		
		psm.executeUpdate();
		
		psm.close();
		
		
		PreparedStatement psc = con.getConnection().prepareStatement(queryCuoio);
		
		psc.executeUpdate();
		
		psc.close();
		
	}
	
	public String aggiornaMinuteria(Integer idProdotto) throws SQLException{
		

		int idMinuteria = 0;
		float mnQuantita = 0;
		int i = 1;
				
		int c = 1;
		
		int result = 0;
		
		boolean flag = true;
		
		String SqlQuery = "";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		String sqlQueryUpdate = "SELECT idMinuteria1, mnQuantita1, idMinuteria2, mnQuantita2, idMInuteria3, mnQuantita3 FROM prodotto where idProdotto = '"+idProdotto+"';";
		//System.out.println(sqlQueryUpdate);
		PreparedStatement psu = con.getConnection().prepareStatement(sqlQueryUpdate);
		
		ResultSet rsu = psu.executeQuery();
		
		System.out.println(sqlQueryUpdate);
		
		if(rsu.next()) {
		
			while(c<=5  && flag)
			{
				idMinuteria = rsu.getInt(c);
				mnQuantita = rsu.getFloat(c+1);
				
				
				
				String SqlQueryTest = "SELECT mnQuantita FROM minuteria WHERE idMinuteria = '" +idMinuteria+ "';";
				
				System.out.println(SqlQueryTest);
				
				PreparedStatement pst = con.getConnection().prepareStatement(SqlQueryTest);
				
				ResultSet rs = pst.executeQuery();
				
				if(rs.next()) {
					result = rs.getInt(1);
					System.out.println(result);
				}
				
				if(result <= mnQuantita) {
					flag = false;
				}else if(idMinuteria != 0){
					SqlQuery = SqlQuery +"UPDATE minuteria SET mnQuantita = mnQuantita - '"+mnQuantita+"' WHERE idMinuteria = '"+idMinuteria+"';";
				}
				
				c = c + 2;
				
			}
		
		}
		
		if(flag) {

			System.out.println(SqlQuery);
			
			
		}else {
			SqlQuery = null;
		}	
		
		psu.close();
		
		if(SqlQuery == null) {
			//aggiungere un alert tutto ok
			showMessageDialog(null, "Tabella minuteria non aggiornata, materiali in esaurimento");
		}
		
		
		return SqlQuery;
		
	}
	
	public String aggiornaCuoio(Integer idProdotto) throws SQLException {

		int idCuoio = 0;
		float cuQuantita = 0;
		int i = 1;
				
		int c = 1;
		
		int result = 0;
		
		boolean flag = true;
		
		String SqlQuery = "";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		String sqlQueryUpdate = "SELECT idCuoio1, cuQuantita1, idCuoio2, cuQuantita2, idCuoio3, cuQuantita3 FROM prodotto WHERE idProdotto = '"+idProdotto+"';";
		//System.out.println(sqlQueryUpdate);
		PreparedStatement psu = con.getConnection().prepareStatement(sqlQueryUpdate);
		
		ResultSet rsu = psu.executeQuery();
		
		System.out.println(sqlQueryUpdate);
		
		if(rsu.next()) {
		
			while(c<=5 && flag)
			{
				idCuoio = rsu.getInt(c);
				cuQuantita = rsu.getFloat(c+1);
				
				String SqlQueryTest = "SELECT cuQuantita FROM cuoio WHERE idCuoio = '" +idCuoio+ "';";
				
				PreparedStatement pst = con.getConnection().prepareStatement(SqlQueryTest);
				
				ResultSet rs = pst.executeQuery();
				if(rs.next()) {
					result = rs.getInt(1);
				}
				
				if(result <= cuQuantita) {
					flag = false;
				}else if(idCuoio != 0){
					SqlQuery = SqlQuery +"UPDATE cuoio SET cuQuantita = cuQuantita - '"+cuQuantita+"' WHERE idCuoio = '"+idCuoio+"';";
				}

				c = c + 2;
				
				pst.close();
				
			}
		}
		
		if(flag) {

			System.out.println(SqlQuery);
			
		}else {
			SqlQuery = null;
		}
		
		psu.close();
		
		if(SqlQuery == null) {
			//aggiungere un alert tutto ok
			showMessageDialog(null, "Tabella cuoio non aggiornata, materiali in esaurimento");
		}
		
		return SqlQuery;
	}

}
