/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

import database.ConnessioneDatabaseMySql;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 *
 * @author loren
 */


public class listaOrdiniAttivi extends JFrame {

	private JPanel contentPane;
	private JTable tableOrdiniAttivi;
	private JTextField dataConsegnatxtField;
	private JTextField idOrdineTextField;
	

	/**
	 * Create the frame.
	 */
	public listaOrdiniAttivi() {
		setTitle("Lista ordini attivi");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 1142, 480);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 1108, 262);
		contentPane.add(scrollPane);
		
		tableOrdiniAttivi = new JTable();
		tableOrdiniAttivi.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id Ordine", "Data ordine", "Nome prodotto", "Nome cliente", "id Preventivo", "Data Consegna", "Consegnato"
			}
		));
		scrollPane.setViewportView(tableOrdiniAttivi);
		
		JLabel dataConsegnaLabel = new JLabel("Inserire data consegna");
		dataConsegnaLabel.setBounds(172, 322, 118, 14);
		contentPane.add(dataConsegnaLabel);
		
		dataConsegnatxtField = new JTextField();
		dataConsegnatxtField.setBounds(172, 347, 96, 20);
		contentPane.add(dataConsegnatxtField);
		dataConsegnatxtField.setColumns(10);
		
		
		
		JLabel lblNewLabel = new JLabel("Ordine consegnato");
		lblNewLabel.setBounds(335, 322, 112, 14);
		contentPane.add(lblNewLabel);
		
		JButton consegnaButton = new JButton("Consegnato");
		consegnaButton.setBackground(Color.WHITE);
		consegnaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					modificaConsegnato(idOrdineTextField);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		consegnaButton.setBounds(335, 346, 112, 23);
		contentPane.add(consegnaButton);
		
		JLabel idOrdineLabel = new JLabel("Inserire idOrdine");
		idOrdineLabel.setBounds(10, 322, 104, 14);
		contentPane.add(idOrdineLabel);
		
		idOrdineTextField = new JTextField();
		idOrdineTextField.setBounds(10, 347, 96, 20);
		contentPane.add(idOrdineTextField);
		idOrdineTextField.setColumns(10);
		
		JButton okButton = new JButton("Inserisci");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					modificaDataConsegna(dataConsegnatxtField, idOrdineTextField);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		okButton.setBackground(new Color(255, 255, 255));
		okButton.setBounds(172, 384, 89, 23);
		contentPane.add(okButton);
		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					DefaultTableModel tm = (DefaultTableModel) tableOrdiniAttivi.getModel();
					tm.setRowCount(0);
					retriveOrdini();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(496, 346, 89, 23);
		contentPane.add(btnNewButton);
		
		
		
		/**
		 * 		Vista dei ordini con join cliente, prodotto e preventivo
		 * 
		 * */
		
		try {
			retriveOrdini();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void retriveOrdini() throws SQLException{
		
		String sqlQuery = "SELECT C.idCliente, C.clNome, C.clCognome, I.idProdotto, I.prArticolo, P.idPreventivo, O.* FROM cliente AS C, prodotto AS I, preventivo AS P, ordine AS O " +
							"WHERE C.idCliente = O.idCliente AND I.idProdotto = O.idProdotto AND P.idPreventivo = O.idPreventivo AND orConsegnato = '0' ORDER BY O.orDataConsegna ASC";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			DefaultTableModel m = (DefaultTableModel) tableOrdiniAttivi.getModel();
			String nomeCognome = rs.getString(2)+" "+rs.getString(3);
			m.addRow(new Object[] {rs.getInt(7),rs.getString(8),rs.getString(5),nomeCognome,rs.getInt(11),rs.getString(12),rs.getString(13)});
		}
		
		ps.close();
	}

	
	//NON FUNZIONA DEVO GUARDARCI
	public void modificaDataConsegna(JTextField dataConsegnatxtField2, JTextField idOrdineTextField) throws SQLException {
		String dataConsegna = null;
		String idOrdineModifica = null;
		
		try{dataConsegna = dataConsegnatxtField2.getText();}catch(NumberFormatException e) {};
		try{idOrdineModifica = idOrdineTextField.getText();}catch(NumberFormatException e) {};
		
		int id = Integer.parseInt(idOrdineModifica);
		
		String sqlQuery = "UPDATE ordine SET orDataConsegna = '"+dataConsegna+"' WHERE idOrdine = '"+id+"';";
		//System.out.println(sqlQuery);
	
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ps.executeUpdate();
		
		ps.close();
		
	}
	
	public void modificaConsegnato( JTextField idOrdineTextField) throws SQLException{
		String idOrdineModifica = null;
		
		try{idOrdineModifica = idOrdineTextField.getText();}catch(NumberFormatException e) {};
		
		int id = Integer.parseInt(idOrdineModifica);
		
		String sqlQuery = "UPDATE ordine SET orConsegnato = '1' WHERE idOrdine = '"+id+"';";
		//System.out.println(sqlQuery);
	
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		ps.executeUpdate();
		
		ps.close();
		
	}
}
