/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;
import magazzino.Cuoio;
import magazzino.Minuteria;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;

import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;


/**
 *
 * @author loren
 */
public class InterfacciaNuovoPreventivo extends JFrame {

	private JPanel contentPane;
	private JTextField txtNomeCliente, txtCognome, txtContatto, txtNumPreventivo, txtData, txtNomeProdotto, txtSconto;
	private JTextField txtCuoio1, txtCuoio2, txtCuoio3;
	private JTextField txtQuantitaC1, txtQuantitaC2, txtQuantitaC3;
	private JTextField txtPrezzoC1, txtPrezzoC2, txtPrezzoC3;
	private JTextField txtMinuteria1, txtMinuteria2, txtMinuteria3;
	private JTextField txtQuantitaM1, txtQuantitaM2, txtQuantitaM3;
	private JTextField txtPrezzoM1, txtPrezzoM2, txtPrezzoM3;
	private JTextField txtTipo1, txtTipo2, txtTipo3;
	private JTextField txtFinitura1, txtFinitura2, txtFinitura3;

	
	private Cuoio cu1, cu2, cu3, cu4;
	private Minuteria mn1, mn2, mn3, mn4, mn5;
	private String nomeCliente, cognome, data, nomeProdotto, contatto;
	private Integer numeroPreventivo;
	private Float subtotale, totale;
	private Float sconto;
	
	private String itemSelected;
	private JTextField txtSubtotale;
	private JTextField txtTotale;
	private JTextField txtNumProdotti;
	
	
	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public InterfacciaNuovoPreventivo() throws SQLException {
		
		mn1 = new Minuteria();
		mn2 = new Minuteria();
		mn3 = new Minuteria();
		mn4 = new Minuteria();
		mn5 = new Minuteria();
		
		cu1 = new Cuoio();
		cu2 = new Cuoio();
		cu3 = new Cuoio();
		cu4 = new Cuoio();
		
                
		
		setTitle("Nuovo preventivo");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 1039, 564);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCliente = new JLabel("Nome cliente");
		lblCliente.setBounds(10, 14, 88, 14);
		contentPane.add(lblCliente);
		
		JLabel lblNmPreventivo = new JLabel("Codice preventivo");
		lblNmPreventivo.setBounds(325, 65, 111, 14);
		contentPane.add(lblNmPreventivo);
		
		JLabel lblData = new JLabel("Data");
		lblData.setBounds(325, 14, 88, 14);
		contentPane.add(lblData);
		
		JLabel lblContatto = new JLabel("Telefono");
		lblContatto.setBounds(10, 65, 88, 14);
		contentPane.add(lblContatto);
		
		txtNomeCliente = new JTextField();
		txtNomeCliente.setBounds(108, 11, 188, 20);
		contentPane.add(txtNomeCliente);
		txtNomeCliente.setColumns(10);
		
		txtNumPreventivo = new JTextField();
		txtNumPreventivo.setBounds(469, 62, 188, 20);
		contentPane.add(txtNumPreventivo);
		txtNumPreventivo.setColumns(10);
		
		txtContatto = new JTextField();
		txtContatto.setBounds(108, 62, 188, 20);
		contentPane.add(txtContatto);
		txtContatto.setColumns(10);
		
		JLabel lblSubtotale = new JLabel("Subtotale");
		lblSubtotale.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSubtotale.setBounds(794, 411, 79, 14);
		contentPane.add(lblSubtotale);
		
		JLabel lblSconto = new JLabel("Sconto");
		lblSconto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSconto.setBounds(794, 442, 79, 14);
		contentPane.add(lblSconto);
		
		JLabel lblTotale = new JLabel("Totale");
		lblTotale.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTotale.setBounds(794, 475, 79, 14);
		contentPane.add(lblTotale);
		
		txtData = new JTextField();
		txtData.setBounds(469, 11, 188, 20);
		contentPane.add(txtData);
		txtData.setColumns(10);
		
		JLabel lblImportaProdotto = new JLabel("Importa prodotto");
		lblImportaProdotto.setBounds(730, 14, 139, 14);
		contentPane.add(lblImportaProdotto);
				
		JLabel lblCuoio = new JLabel("Cuoio");
		lblCuoio.setBounds(166, 179, 49, 14);
		contentPane.add(lblCuoio);
		
		txtCuoio1 = new JTextField();
		txtCuoio1.setBounds(258, 176, 96, 20);
		contentPane.add(txtCuoio1);
		txtCuoio1.setColumns(10);
		
		txtCuoio2 = new JTextField();
		txtCuoio2.setBounds(258, 207, 96, 20);
		contentPane.add(txtCuoio2);
		txtCuoio2.setColumns(10);
		
		txtCuoio3 = new JTextField();
		txtCuoio3.setBounds(258, 238, 96, 20);
		contentPane.add(txtCuoio3);
		txtCuoio3.setColumns(10);
		
		JLabel lblQuantita = new JLabel("Quantit\u00E0 (m^2)");
		lblQuantita.setBounds(602, 151, 96, 14);
		contentPane.add(lblQuantita);
		
		txtQuantitaC1 = new JTextField();
		txtQuantitaC1.setBounds(602, 176, 96, 20);
		contentPane.add(txtQuantitaC1);
		txtQuantitaC1.setColumns(10);
		
		txtQuantitaC2 = new JTextField();
		txtQuantitaC2.setBounds(602, 207, 96, 20);
		contentPane.add(txtQuantitaC2);
		txtQuantitaC2.setColumns(10);
		
		txtQuantitaC3 = new JTextField();
		txtQuantitaC3.setBounds(602, 238, 96, 20);
		contentPane.add(txtQuantitaC3);
		txtQuantitaC3.setColumns(10);
		
		JLabel lblPrezzo = new JLabel("Prezzo (m^2)");
		lblPrezzo.setBounds(777, 151, 92, 14);
		contentPane.add(lblPrezzo);
		
		txtPrezzoC1 = new JTextField();
		txtPrezzoC1.setBounds(777, 176, 96, 20);
		contentPane.add(txtPrezzoC1);
		txtPrezzoC1.setColumns(10);
		
		txtPrezzoC2 = new JTextField();
		txtPrezzoC2.setBounds(777, 207, 96, 20);
		contentPane.add(txtPrezzoC2);
		txtPrezzoC2.setColumns(10);
		
		txtPrezzoC3 = new JTextField();
		txtPrezzoC3.setBounds(777, 238, 96, 20);
		contentPane.add(txtPrezzoC3);
		txtPrezzoC3.setColumns(10);
		
		JLabel lblMinuteria = new JLabel("Minuteria");
		lblMinuteria.setBounds(166, 297, 73, 14);
		contentPane.add(lblMinuteria);
		
		txtMinuteria1 = new JTextField();
		txtMinuteria1.setBounds(258, 294, 96, 20);
		contentPane.add(txtMinuteria1);
		txtMinuteria1.setColumns(10);
		
		txtMinuteria2 = new JTextField();
		txtMinuteria2.setBounds(258, 325, 96, 20);
		contentPane.add(txtMinuteria2);
		txtMinuteria2.setColumns(10);
		
		txtMinuteria3 = new JTextField();
		txtMinuteria3.setBounds(258, 356, 96, 20);
		contentPane.add(txtMinuteria3);
		txtMinuteria3.setColumns(10);
		
		txtQuantitaM1 = new JTextField();
		txtQuantitaM1.setBounds(602, 294, 96, 20);
		contentPane.add(txtQuantitaM1);
		txtQuantitaM1.setColumns(10);
		
		txtQuantitaM2 = new JTextField();
		txtQuantitaM2.setBounds(602, 325, 96, 20);
		contentPane.add(txtQuantitaM2);
		txtQuantitaM2.setColumns(10);
		
		txtQuantitaM3 = new JTextField();
		txtQuantitaM3.setBounds(602, 356, 96, 20);
		contentPane.add(txtQuantitaM3);
		txtQuantitaM3.setColumns(10);
		
		txtPrezzoM1 = new JTextField();
		txtPrezzoM1.setBounds(777, 294, 96, 20);
		contentPane.add(txtPrezzoM1);
		txtPrezzoM1.setColumns(10);
		
		txtPrezzoM2 = new JTextField();
		txtPrezzoM2.setBounds(777, 325, 96, 20);
		contentPane.add(txtPrezzoM2);
		txtPrezzoM2.setColumns(10);
		
		txtPrezzoM3 = new JTextField();
		txtPrezzoM3.setBounds(777, 356, 96, 20);
		contentPane.add(txtPrezzoM3);
		txtPrezzoM3.setColumns(10);
		
		JLabel lblNomeProdotto = new JLabel("Nome Prodotto");
		lblNomeProdotto.setBounds(325, 39, 79, 14);
		contentPane.add(lblNomeProdotto);
		
		txtNomeProdotto = new JTextField();
		txtNomeProdotto.setBounds(469, 35, 188, 23);
		contentPane.add(txtNomeProdotto);
		txtNomeProdotto.setColumns(10);
		
		txtTipo1 = new JTextField();
		txtTipo1.setBounds(424, 176, 96, 20);
		contentPane.add(txtTipo1);
		txtTipo1.setColumns(10);
		
		txtTipo2 = new JTextField();
		txtTipo2.setBounds(424, 207, 96, 20);
		contentPane.add(txtTipo2);
		txtTipo2.setColumns(10);
		
		txtTipo3 = new JTextField();
		txtTipo3.setBounds(424, 238, 96, 20);
		contentPane.add(txtTipo3);
		txtTipo3.setColumns(10);
		
		txtFinitura1 = new JTextField();
		txtFinitura1.setBounds(424, 294, 96, 20);
		contentPane.add(txtFinitura1);
		txtFinitura1.setColumns(10);
		
		txtFinitura2 = new JTextField();
		txtFinitura2.setBounds(424, 325, 96, 20);
		contentPane.add(txtFinitura2);
		txtFinitura2.setColumns(10);
		
		txtFinitura3 = new JTextField();
		txtFinitura3.setBounds(424, 356, 96, 20);
		contentPane.add(txtFinitura3);
		txtFinitura3.setColumns(10);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(424, 151, 49, 14);
		contentPane.add(lblTipo);
		
		JLabel lblFinitura = new JLabel("Finitura");
		lblFinitura.setBounds(424,269, 49, 14);
		contentPane.add(lblFinitura);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(258, 151, 49, 14);
		contentPane.add(lblNome);
		
		JLabel lblCognome = new JLabel("Cognome ");
		lblCognome.setBounds(10, 39, 79, 14);
		contentPane.add(lblCognome);
		
		txtCognome = new JTextField();
		txtCognome.setBounds(108, 36, 188, 20);
		contentPane.add(txtCognome);
		txtCognome.setColumns(10);
		
		JTextArea textAreaSubtotale = new JTextArea();
		textAreaSubtotale.setBounds(883, 411, 96, 22);
		contentPane.add(textAreaSubtotale);
		
		JTextArea textAreaTotale = new JTextArea();
		textAreaTotale.setBounds(883,475, 96, 22);			
		contentPane.add(textAreaTotale);
		

		txtSconto = new JTextField();
		txtSconto.setBounds(883,442, 96, 20);
		contentPane.add(txtSconto);
		txtSconto.setColumns(10);
		
		
		
		/**
		 * 		Codice importa prodotto
		 * 
		 * */
		JScrollPane listproductPane = new JScrollPane();
		listproductPane.setBounds(730, 39, 249, 89);
		contentPane.add(listproductPane);
		
		String strTemp = "";
		String sqlQuery ="SELECT idProdotto, prArticolo FROM leathercrm.prodotto ORDER BY idProdotto";
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ResultSet rs = ps.executeQuery();
		
		DefaultListModel<String> lm = new DefaultListModel<String>();
		
		while(rs.next()) {
			strTemp = rs.getInt(1) + ": " + rs.getString(2);
			lm.addElement(strTemp);
		}
		
		JList listProduct = new JList();
		listproductPane.setViewportView(listProduct);
		{
			listProduct.setModel(lm);
		}
		
		ps.close();
		
		JButton btnImporta = new JButton("Importa");
		btnImporta.setBackground(new Color(255, 255, 255));
		btnImporta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				itemSelected = (String) listProduct.getSelectedValue();
				//System.out.println(itemSelected);
				try {
					compileTextField();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnImporta.setBounds(890, 10, 89, 23);
		contentPane.add(btnImporta);
		
		
		
		JButton btnBozza = new JButton("Vedi Bozza");
		btnBozza.setBackground(new Color(255, 255, 255));
		btnBozza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				retriveValues();
				float tot = calculateTotale();
				totale = tot;
				textAreaSubtotale.setText(null);
				textAreaTotale.setText(null);;
				textAreaSubtotale.setText(String.valueOf(subtotale));
				textAreaTotale.setText(String.valueOf(totale));
				
			}
		});
		btnBozza.setBounds(10, 466, 146, 41);
		contentPane.add(btnBozza);
                
                JButton btnCreaPreventivo = new JButton("Crea preventivo");
		btnCreaPreventivo.setBackground(new Color(255, 255, 255));
		btnCreaPreventivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					creaPreventivo();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("chiuso");
				dispose();
			}
		});
		btnCreaPreventivo.setBounds(166, 464, 146, 41);
		contentPane.add(btnCreaPreventivo);
		
		JLabel lblPrezzoMn = new JLabel("Prezzo (al pezzo)");
		lblPrezzoMn.setBounds(777, 269, 92, 14);
		contentPane.add(lblPrezzoMn);
		
		JLabel lblQuantitaMn = new JLabel("Quantit\u00E0 (n\u00B0 pezzi)");
		lblQuantitaMn.setBounds(602, 269, 96, 14);
		contentPane.add(lblQuantitaMn);
		
		JLabel lblNewLabel = new JLabel("Numero di prodotti");
		lblNewLabel.setBounds(325, 90, 101, 14);
		contentPane.add(lblNewLabel);
		
		txtNumProdotti = new JTextField();
		txtNumProdotti.setBounds(469, 87, 188, 20);
		contentPane.add(txtNumProdotti);
		txtNumProdotti.setColumns(10);
		
		
				
		
	}
	
	public void retriveValues() {
		try{nomeCliente = txtNomeCliente.getText();}catch(NumberFormatException e) {};
		try{cognome = txtCognome.getText();}catch(NumberFormatException e) {};
		try{data = txtData.getText();}catch(NumberFormatException e) {};
		try{nomeProdotto = txtNomeProdotto.getText();}catch(NumberFormatException e) {};
		try{contatto = txtContatto.getText();}catch(NumberFormatException e) {};
		try{numeroPreventivo = Integer.parseInt(txtNumPreventivo.getText());}catch(NumberFormatException e) {};
		
		/**
		 * 		Recupero dai textfield i valori per il cuoio
		 * */
		try {cu1.setArticolo(txtCuoio1.getText());}catch(NumberFormatException e) {};
		try {cu2.setArticolo(txtCuoio2.getText());}catch(NumberFormatException e) {};
		try {cu3.setArticolo(txtCuoio3.getText());}catch(NumberFormatException e) {};
		
		try {cu1.setTipoCuoio(txtTipo1.getText());}catch(NumberFormatException e) {};
		try {cu2.setTipoCuoio(txtTipo2.getText());}catch(NumberFormatException e) {};
		try {cu3.setTipoCuoio(txtTipo3.getText());}catch(NumberFormatException e) {};
		
		try {cu1.setQuantita(Float.parseFloat(txtQuantitaC1.getText()));}catch(NumberFormatException e) {};
		try {cu2.setQuantita(Float.parseFloat(txtQuantitaC2.getText()));}catch(NumberFormatException e) {};
		try {cu3.setQuantita(Float.parseFloat(txtQuantitaC3.getText()));}catch(NumberFormatException e) {};
		
		try {cu1.setCosto(Float.parseFloat(txtPrezzoC1.getText()));}catch(NumberFormatException e) {};
		try {cu2.setCosto(Float.parseFloat(txtPrezzoC2.getText()));}catch(NumberFormatException e) {};
		try {cu3.setCosto(Float.parseFloat(txtPrezzoC3.getText()));}catch(NumberFormatException e) {};
		
		
		/**
		 * 		Recupero dai textfield i valori per le minuetrie
		 * */
		try {mn1.setArticolo(txtMinuteria1.getText());}catch(NumberFormatException e) {};
		try {mn2.setArticolo(txtMinuteria2.getText());}catch(NumberFormatException e) {};
		try {mn3.setArticolo(txtMinuteria3.getText());}catch(NumberFormatException e) {};
		
		try {mn1.setFinitura(txtFinitura1.getText());}catch(NumberFormatException e) {};
		try {mn2.setFinitura(txtFinitura2.getText());}catch(NumberFormatException e) {};
		try {mn3.setFinitura(txtFinitura3.getText());}catch(NumberFormatException e) {};
		
		try {mn1.setQuantita(Float.parseFloat(txtQuantitaM1.getText()));}catch(NumberFormatException e) {};
		try {mn2.setQuantita(Float.parseFloat(txtQuantitaM2.getText()));}catch(NumberFormatException e) {};
		try {mn3.setQuantita(Float.parseFloat(txtQuantitaM3.getText()));}catch(NumberFormatException e) {};
		
		try {mn1.setCosto(Float.parseFloat(txtPrezzoM1.getText()));}catch(NumberFormatException e) {};
		try {mn2.setCosto(Float.parseFloat(txtPrezzoM2.getText()));}catch(NumberFormatException e) {};
		try {mn3.setCosto(Float.parseFloat(txtPrezzoM3.getText()));}catch(NumberFormatException e) {};
		
	}
	
	public Float calculateTotale() {
		Float totale;
		Float totaleCuoio = 0f;
		Float totaleMinuteria = 0f;
		Integer nProdotti = 1;
		
		try{nProdotti = Integer.parseInt(txtNumProdotti.getText());}catch(NumberFormatException e) {};
		
		try{sconto = Float.parseFloat(txtSconto.getText());}catch(NumberFormatException e) {};
		
		if(cu1.getQuantita() != null)
			totaleCuoio += cu1.getQuantita()*cu1.getCosto();
		if(cu2.getQuantita() != null)
			totaleCuoio += cu2.getQuantita()*cu2.getCosto();
		if(cu3.getQuantita() != null)
			totaleCuoio += cu3.getQuantita()*cu3.getCosto();
			
		
		if(mn1.getQuantita() != null)
			totaleMinuteria += mn1.getQuantita()*mn1.getCosto();
		if(mn2.getQuantita() != null)
			totaleMinuteria += mn2.getQuantita()*mn2.getCosto();
		if( mn3.getQuantita() != null)
			totaleMinuteria += mn3.getQuantita()*mn3.getCosto();
			
		subtotale = totaleCuoio + totaleMinuteria;
		//System.out.println(sconto);
		if(sconto != null)
			if(sconto != 0) {
				totale = 0f;
				totale = subtotale - (sconto/100)*subtotale;	
				totale = totale*nProdotti;
			}
			else
				totale = nProdotti*subtotale;
		else
			totale = nProdotti*subtotale;
                
                return totale;
		
	}
	
	public void compileTextField() throws SQLException {
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		String sqlQuery1 = null;
		String sqlQuery2 = null;
		String sqlQuery3 = null;
		
		String [] arrayTemp = new String[1];
		arrayTemp = itemSelected.split(":");
		
		
		//System.out.println(arrayTemp[0]);
		
		Integer index = Integer.parseInt(arrayTemp[0]);
		
		String sqlQuery0 = "SELECT idCuoio1, idCuoio2, idCuoio3, idMinuteria1, idMinuteria2, idMinuteria3 FROM prodotto WHERE idProdotto='" +index+"';";
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery0);
		ResultSet rs0 = ps.executeQuery();
		
		if(rs0.next()) {
			if(rs0.getInt(1) != 0 && rs0.getInt(4) != 0)
				sqlQuery1 ="SELECT idProdotto, cuQuantita1, mnQuantita1, idCuoio, cuNome, cuTipo, cuResa, idMinuteria, mnNome, mnFinitura, mnResa FROM cuoio AS C, minuteria AS M, prodotto AS P "+ 
									"WHERE C.idCuoio = '" +rs0.getInt(1)+ "' AND M.idMinuteria = '" +rs0.getInt(4)+ "' AND P.idProdotto = '" +index+ "';";
			
			if(rs0.getInt(2) != 0 && rs0.getInt(5) != 0)
				sqlQuery2 ="SELECT idProdotto, cuQuantita2, mnQuantita2, idCuoio, cuNome, cuTipo, cuResa, idMinuteria, mnNome, mnFinitura, mnResa FROM cuoio AS C, minuteria AS M, prodotto AS P "+ 
									"WHERE C.idCuoio = '" +rs0.getInt(2)+ "' AND M.idMinuteria = '" +rs0.getInt(5)+ "' AND P.idProdotto = '" +index+ "';";
			
			if(rs0.getInt(3) != 0 && rs0.getInt(6) != 0)
				sqlQuery3 ="SELECT idProdotto, cuQuantita3, mnQuantita3, idCuoio, cuNome, cuTipo, cuResa, idMinuteria, mnNome, mnFinitura, mnResa FROM cuoio AS C, minuteria AS M, prodotto AS P "+ 
									"WHERE C.idCuoio = '" +rs0.getInt(3)+ "' AND M.idMinuteria = '" +rs0.getInt(6)+ "' AND P.idProdotto = '" +index+ "';";
		}
		
		System.out.println(sqlQuery1);
		System.out.println(sqlQuery2);
		System.out.println(sqlQuery3);
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		PreparedStatement ps3 = null;
		
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		
		if(sqlQuery1 != null) {
			 ps1 = con.getConnection().prepareStatement(sqlQuery1);
			 rs1 = ps1.executeQuery();
		}
		
		if(sqlQuery2 != null) {
			ps2 = con.getConnection().prepareStatement(sqlQuery2);
			rs2 = ps2.executeQuery();
		}
		
		if(sqlQuery3 != null) {
			ps3 = con.getConnection().prepareStatement(sqlQuery3);
			rs3 = ps3.executeQuery();
		}
		
		try {
			if(rs1.next()) {
				txtCuoio1.setText(rs1.getString(5));
				txtTipo1.setText(rs1.getString(6));
				txtQuantitaC1.setText(String.valueOf(rs1.getFloat(2)));
				txtPrezzoC1.setText(String.valueOf(rs1.getFloat(7)));
				
				txtMinuteria1.setText(rs1.getString(9));
				txtFinitura1.setText(rs1.getString(10));
				txtQuantitaM1.setText(String.valueOf(rs1.getFloat(3)));
				txtPrezzoM1.setText(String.valueOf(rs1.getFloat(11)));
				
				//System.out.println(rs1.getString(2) + " AND " +rs1.getString(7));
			}
			
			if(rs2.next()) {
				txtCuoio2.setText(rs2.getString(5));
				txtTipo2.setText(rs2.getString(6));
				txtQuantitaC2.setText(String.valueOf(rs2.getFloat(2)));
				txtPrezzoC2.setText(String.valueOf(rs2.getFloat(7)));
				
				txtMinuteria2.setText(rs2.getString(9));
				txtFinitura2.setText(rs2.getString(10));
				txtQuantitaM2.setText(String.valueOf(rs2.getFloat(3)));
				txtPrezzoM2.setText(String.valueOf(rs2.getFloat(11)));
			}
			
			if(rs3.next()) {
				txtCuoio3.setText(rs3.getString(5));
				txtTipo3.setText(rs3.getString(6));
				txtQuantitaC3.setText(String.valueOf(rs3.getFloat(2)));
				txtPrezzoC3.setText(String.valueOf(rs3.getFloat(7)));
				
				txtMinuteria3.setText(rs3.getString(9));
				txtFinitura3.setText(rs3.getString(10));
				txtQuantitaM3.setText(String.valueOf(rs3.getFloat(3)));
				txtPrezzoM3.setText(String.valueOf(rs3.getFloat(11)));
			}
		}
		catch(NullPointerException n) {};
		
		ps.close();
	}
	
	/**
	 * 
	 * 		Metodo per popolare la tabella preventivo con i dati inseriti nella form
	 * 
	 * 
	 * */
	
	public void creaPreventivo() throws SQLException {
		
		
		Integer nProdotti = 1;
		
		try {nProdotti = Integer.parseInt(txtNumProdotti.getText());}catch(NumberFormatException e) {};
		if(sconto == null)
			sconto = 0f;
		
		int idcliente = 0;
				
		String sqlRetriveClient = "SELECT idCliente FROM cliente AS C WHERE C.clNome = '" +txtNomeCliente.getText()+ "' AND C.clCognome = '" +txtCognome.getText()+
																										"' AND C.clNTelefono = '" +txtContatto.getText()+ "';";
		
		
		String sqlCreateClient = "INSERT INTO cliente (clNome, clCognome, clNTelefono) VALUES ('"+txtNomeCliente.getText()+"','"+txtCognome.getText()+"','"+txtContatto.getText()+"');";
		
		boolean flag = false;
		
		
		
//		System.out.println(sqlRetriveClient);
//		System.out.println(sqlCreateClient);
		
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		PreparedStatement ps = con.getConnection().prepareStatement(sqlRetriveClient);
		ResultSet rs, rs10;
		rs = null;
		rs10 = null;
		
		rs = ps.executeQuery();
		
		if(!rs.next()) {
			flag = true;
		}
		else {
			idcliente = rs.getInt(1);
		}

		/**
		 * 		Crea la riga cliente che non c'era prima e recupera l'id
		 * */
		
		if(flag) {
			ps = con.getConnection().prepareStatement(sqlCreateClient);
			int sqlOK = ps.executeUpdate();
			
			ps = con.getConnection().prepareStatement(sqlRetriveClient);
			rs10 = ps.executeQuery();
			
		}
		
		
		String [] arrayTemp = new String[1];
		arrayTemp = itemSelected.split(":");
		
		try {
			if(rs10.next())
				idcliente = rs10.getInt(1);
		}catch(NullPointerException e) {};
		/**
		 * 		Popola la tabella preventivo
		 * */

		String sqlCreatePreventivo = "INSERT INTO preventivo (pvData, pvTotale, pvSconto, pvNumeroPreventivo, pvContatto, pvStatus, idCliente, idProdotto,nProdotti) "+
									"VALUES ('" +txtData.getText()+ "','" +totale+ "','" +sconto+ "','" +txtNumPreventivo.getText()+ "','" +txtContatto.getText()+ "','attivo','" +idcliente+ "','" +arrayTemp[0]+ "','"+nProdotti+"');";
		
		//System.out.println(sqlCreatePreventivo);
		
		ps = con.getConnection().prepareStatement(sqlCreatePreventivo);
		ps.executeUpdate();
		ps.close();
		
	}
}

