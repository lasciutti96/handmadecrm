/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.ConnessioneDatabaseMySql;

import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;
/**
 *
 * @author loren
 */


public class deleteInterfaccia extends JFrame {

	private JPanel contentPane;
	private JTextField txtId;
	protected String nomeTabella;


	/**
	 * Create the frame.
	 */
	public deleteInterfaccia() {
		setTitle("Elimina");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 384, 413);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rdbtnCuoio = new JRadioButton("Cuoio");
		rdbtnCuoio.setBackground(new Color(175, 238, 238));
		rdbtnCuoio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = "cuoio";
			}
		});
		rdbtnCuoio.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnCuoio.setBounds(6, 45, 111, 23);
		contentPane.add(rdbtnCuoio);
		
		JRadioButton rdbtnChimici = new JRadioButton("Chimici");
		rdbtnChimici.setBackground(new Color(175, 238, 238));
		rdbtnChimici.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = "chimici";
			}
		});
		rdbtnChimici.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnChimici.setBounds(6, 71, 111, 23);
		contentPane.add(rdbtnChimici);
		
		JRadioButton rdbtnMinuteria = new JRadioButton("Minuteria");
		rdbtnMinuteria.setBackground(new Color(175, 238, 238));
		rdbtnMinuteria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = "minuteria";
			}
		});
		rdbtnMinuteria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnMinuteria.setBounds(6, 97, 111, 23);
		contentPane.add(rdbtnMinuteria);
		
		JRadioButton rdbtnAttrezzi = new JRadioButton("Attrezzi");
		rdbtnAttrezzi.setBackground(new Color(175, 238, 238));
		rdbtnAttrezzi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = "attrezzi";
			}
		});
		rdbtnAttrezzi.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnAttrezzi.setBounds(6, 123, 111, 23);
		contentPane.add(rdbtnAttrezzi);
		
		JRadioButton rdbtnMacchinari = new JRadioButton("Macchinari");
		rdbtnMacchinari.setBackground(new Color(175, 238, 238));
		rdbtnMacchinari.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nomeTabella = "macchinari";
			}
		});
		rdbtnMacchinari.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnMacchinari.setBounds(6, 149, 111, 23);
		contentPane.add(rdbtnMacchinari);
		
		ButtonGroup gruppoTab = new ButtonGroup();
		gruppoTab.add(rdbtnCuoio);
		gruppoTab.add(rdbtnChimici);
		gruppoTab.add(rdbtnMinuteria);
		gruppoTab.add(rdbtnAttrezzi);
		gruppoTab.add(rdbtnMacchinari);
		
		
		JLabel lblNewLabel = new JLabel("Selezionare quale tabella modificare");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 7, 269, 31);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Specificare l'id della riga che vuoi eliminare");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(10, 179, 287, 33);
		contentPane.add(lblNewLabel_1);
		
		txtId = new JTextField();
		txtId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtId.setBounds(6, 223, 134, 30);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		JButton delButton = new JButton("Elimina");
		delButton.setBackground(new Color(255, 255, 255));
		delButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//esegue la query per eliminare la riga selezionata dall'id
				
				String sqlQuery = null;
				Integer idField = Integer.parseInt(txtId.getText());
				switch(nomeTabella) {
					
				case("cuoio"):
					sqlQuery = "DELETE FROM leathercrm.cuoio WHERE idCuoio='"+idField+"';";
					
					break;
				
				case("chimici"):
					sqlQuery = "DELETE FROM leathercrm.chimici WHERE idChimici='"+idField+"';";				
					break;
				
				case("minuteria"):
					sqlQuery = "DELETE FROM leathercrm.minuteria WHERE idMinuteria='"+idField+"';";
					break;
				
				case("attrezzi"):
					sqlQuery = "DELETE FROM leathercrm.attrezzi WHERE idAttrezzi='"+idField+"';";
					break;
				
				case("macchinari"):
					sqlQuery = "DELETE FROM leathercrm.macchinari WHERE idMacchinari='"+idField+"';";
					break;
				}
				
				try {
					delRowDatabase(sqlQuery);
					//alert se l'eleminazione è andata a buon fine
					showMessageDialog(null, "Riga eliminata correttamente");
					
					
					//chiusura form
					dispose();
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				
			}
		});
		delButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		delButton.setBounds(77, 322, 96, 43);
		contentPane.add(delButton);
		
		JButton nullButton = new JButton("Annulla");
		nullButton.setBackground(new Color(255, 255, 255));
		nullButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		nullButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		nullButton.setBounds(201, 322, 96, 43);
		contentPane.add(nullButton);
	}
	
	public void delRowDatabase(String sqlQuery) throws SQLException {
		ConnessioneDatabaseMySql con = new ConnessioneDatabaseMySql();
		
		PreparedStatement ps = con.getConnection().prepareStatement(sqlQuery);
		
		ps.executeUpdate();
		
		ps.close();
	}
}
