/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

/**
 *
 * @author loren
 */
public class InterfacciaNuovoPreventivoTest {
    
    public InterfacciaNuovoPreventivoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of retriveValues method, of class InterfacciaNuovoPreventivo.
     */
    @Disabled
    @Test
    public void testRetriveValues() {
        System.out.println("retriveValues");
        InterfacciaNuovoPreventivo instance = null;
        try {
            instance = new InterfacciaNuovoPreventivo();
        } catch (SQLException ex) {
            Logger.getLogger(InterfacciaNuovoPreventivoTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.retriveValues();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculateTotale method, of class InterfacciaNuovoPreventivo.
     */
    @Test
    public void testCalculateTotale() throws SQLException {
        System.out.println("calculateTotale");
        InterfacciaNuovoPreventivo instance = new InterfacciaNuovoPreventivo();
        instance.calculateTotale();
        float expectedValue = 0;
        assertEquals(expectedValue, instance.calculateTotale());
    }

    /**
     * Test of compileTextField method, of class InterfacciaNuovoPreventivo.
     */
    @Disabled
    @Test
    public void testCompileTextField() throws Exception {
        System.out.println("compileTextField");
        InterfacciaNuovoPreventivo instance = new InterfacciaNuovoPreventivo();
        instance.compileTextField();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of creaPreventivo method, of class InterfacciaNuovoPreventivo.
     */
    @Disabled
    @Test
    public void testCreaPreventivo() throws Exception {
        System.out.println("creaPreventivo");
        InterfacciaNuovoPreventivo instance = new InterfacciaNuovoPreventivo();
        instance.creaPreventivo();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
