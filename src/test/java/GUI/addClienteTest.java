/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import database.ConnessioneDatabaseMySql;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;

/**
 *
 * @author loren
 */
public class addClienteTest {
    
    public addClienteTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of aggiungiCliente method, of class addCliente.
     */
    @Test
    public void testAggiungiCliente() throws Exception {
        System.out.println("aggiungiCliente");
        String nome = "Sonia";
        String cognome = "Nicoletti";
        String telefono = "123456789";
        String email = "sonianicoletti7@gmail.com";
        String indirizzo = "Via Bolzano 35";
        String citta = "Chiaravalle";
        addCliente instance = new addCliente();
        boolean expResult = true;
        boolean result = instance.aggiungiCliente(nome, cognome, telefono, email, indirizzo, citta);
        assertEquals(expResult, result);
        
    }    
}
